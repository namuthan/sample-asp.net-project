﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewSemester.aspx.cs" Inherits="Professor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
     <form id="form1" runat="server">
        <div style="height: 728px; margin-left: 120px; width: 1734px;">
            <asp:Panel ID="Panel5" runat="server" Height="55px">
                <asp:Panel ID="Panel3" runat="server" Height="45px">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="MS Reference Sans Serif" Font-Size="XX-Large" Text="A+ Student Planner - Web"></asp:Label>
                    <br />
                </asp:Panel>
                           <br />
             <asp:Panel ID="Panel2" runat="server" Height="16px" Width="1400px" Style="float: left;">
                     <asp:LinkButton ID="LinkButton1" runat="server" Style="margin-left:0px;" OnClick="HomeButton1_Click" Font-Names="Microsoft Sans Serif" Font-Underline="False" ForeColor="Black">Home</asp:LinkButton>
                    <br />
                </asp:Panel>
            <br />
                <asp:Panel ID="Panel4" runat="server" Height="16px" Style="float: left;">
                    <hr style="width: 800px; height: -12px;" />
                    <br />
                       <br />
                </asp:Panel>
             <br />
                <asp:Panel ID="Panel10" runat="server" Height="42px" Style="float: left;" Width="200px">
                        <br />

                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Names="MS Reference Sans Serif" Font-Size="Large" Text="View Semester"></asp:Label>
                      <br />
                </asp:Panel>
            </asp:Panel>
  <br />
                <br />
            <asp:Panel ID="Panel7" runat="server" Height="89px" Width="1393px">
                <br />
                <br />
                <br />
            </asp:Panel>
            <asp:Panel ID="Panel1" runat="server" Height="413px" Width="1291px"  style="margin-left:80px;">
                <asp:Label ID="Label5" runat="server" Text="Semesters:" Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                  <asp:DropDownList ID="SemesterDropDown" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Height="25px" style="margin-left: 0px;" Width="400px">
                </asp:DropDownList>
                  <br />
                <br />
                <asp:Button ID="Button2" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" OnClick="Button1_Click" style="margin-left:450px;" Text="Submit" Width="110px" Height="34px" />
                <br />
                <br />

                  <asp:Label ID="Label9" runat="server" Text="Start Date:" Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                 <asp:Label ID="startDate" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="yyyy-mm-dd" Width="150px"  Style="margin-left:0px;"></asp:Label>
                 <br />
                <br />
                <asp:Label ID="Label10" runat="server" Text="End Date:"  Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:Label ID="endDate" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="yyyy-mm-dd" Width="150px"  Style="margin-left:0px;"></asp:Label>
                <br />
                <br />
                 <asp:Label ID="Label4" runat="server" Text="Classes:" Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                  <br />
                <asp:TextBox ID="classList" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Height="177px" TextMode="MultiLine" Width="400px" style="margin-left:150px;"></asp:TextBox>
                <br />
                <br />
                <br />
                <br />

            </asp:Panel>
                 <asp:Panel ID="Panel9" runat="server" Height="16px" Style="float:left;" Width="594px">
                    <hr style="width: 800px; float:left; margin-left:0px;"/>
                    <br />
                </asp:Panel>
             <asp:Panel ID="Panel11" runat="server" Height="16px" Style="float:left;" Width="986px">
                <br />
                    <br />
                </asp:Panel>
                         <asp:Panel ID="Panel8" runat="server" Height="50px" Width="1393px" style="margin-left:180px;">
                <br />
            </asp:Panel>
<br />

        </div>
    </form>
</body>
</html>
