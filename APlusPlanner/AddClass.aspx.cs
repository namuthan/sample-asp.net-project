﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using MySql.Data;
using DayPilot;
using DayPilot.Web.Ui.Recurrence;

public partial class Professor : System.Web.UI.Page
{
    public MySqlConnection conn;
    public MySqlDataReader reader;
    public MySqlDataAdapter adapter;
    public int id;

    public String redirect;
    String recurrence;

    protected void Page_Load(object sender, EventArgs e)
    {
        
            if (Session["LoggedIn"] == null || Session["LoggedIn"] != "1")
            {
                Response.Redirect("LoginView.aspx");
            }
            conn = new MySqlConnection();
            conn.ConnectionString = "server=70.75.191.26; user id=root; password=; database=aplusplus;Allow Zero Datetime=True";
            conn.Open();

            String query;
            query = "select * from semester where username='" + Session["user"].ToString() + "';";

            MySqlCommand cmd = new MySqlCommand(query, conn);
            reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                SemesterDropDown.Items.Add(reader.GetString(reader.GetOrdinal("name")));
            }

            reader.Close();
        
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        String isMonday = "false";
        String isTuesday = "false";
        String isWednesday = "false";
        String isThursday = "false";
        String isFriday = "false";
        String isSaturday = "false";
        String isSunday = "false";
        String ClassName = eventName.Text;
        String prof = profBox.Text;
        String semester = SemesterDropDown.SelectedValue.ToString();

        
        MySqlCommand cmd;

        String user = Session["user"].ToString();
        String selectLast = "SELECT id FROM userevents ORDER BY ID DESC LIMIT 1;";
        cmd = new MySqlCommand(selectLast, conn);
        reader = cmd.ExecuteReader();
        reader.Read();
        id = reader.GetInt32(0);
        String eventId = id.ToString();
        reader.Close();

        conn = new MySqlConnection();
        conn.ConnectionString = "server=70.75.191.26; user id=root; password=; database=aplusplus";
        conn.Open();

        String query;
        query = "select * from semester where username='" + Session["user"].ToString() + "' AND name='" + semester +  "';";

        cmd = new MySqlCommand(query, conn);
        reader = cmd.ExecuteReader();

        reader.Read();

        

        String startyear;
        String startmonth;
        String startday;
        String endyear;
        String endday;
        String endmonth;


        startyear = reader.GetString(reader.GetOrdinal("startyear"));
        startmonth = reader.GetString(reader.GetOrdinal("startmonth"));
        startday = reader.GetString(reader.GetOrdinal("startday"));
        endyear = reader.GetString(reader.GetOrdinal("endyear"));
        endmonth = reader.GetString(reader.GetOrdinal("endmonth"));
        endday = reader.GetString(reader.GetOrdinal("endday"));
        
        
        reader.Close();

        DateTime start = new DateTime(Int32.Parse(startyear), Int32.Parse(startmonth), Int32.Parse(startday), Int32.Parse(eventStartHour.Text), Int32.Parse(eventStartMinute.Text), 0);

        DateTime end = new DateTime(Int32.Parse(startyear), Int32.Parse(startmonth), Int32.Parse(startday), Int32.Parse(eventEndHour.Text), Int32.Parse(eventEndMinute.Text), 0);

        DateTime until = new DateTime(Int32.Parse(endyear), Int32.Parse(endmonth), Int32.Parse(endday), Int32.Parse(eventEndHour.Text), Int32.Parse(eventEndMinute.Text), 0);

        if (CheckMon.Checked)
        {
            isMonday = "true";
            DateTime tempendDate = end;
            DateTime tempDate = start;
            switch (start.DayOfWeek)
            { 
                case DayOfWeek.Monday:
                    break;
                case DayOfWeek.Tuesday:
                    start = start.AddDays(6);
                    end = end.AddDays(6);
                    break;
                case DayOfWeek.Wednesday:
                    start = start.AddDays(5);
                    end = end.AddDays(5);
                    break;
                case DayOfWeek.Thursday:
                    start = start.AddDays(4);
                    end = end.AddDays(4);
                    break;
                case DayOfWeek.Friday:
                    start = start.AddDays(3);
                    end = end.AddDays(3);
                    break;
                case DayOfWeek.Saturday:
                    start = start.AddDays(2);
                    end = end.AddDays(2);
                    break;
                case DayOfWeek.Sunday:
                    start = start.AddDays(1);
                    end = end.AddDays(1);
                    break;
            }
            id += 1;
            eventId = id.ToString();
            recurrence = RecurrenceRule.FromDateTime(id.ToString(), start).Weekly().Until(until).Encode();
            String query2 = "INSERT INTO userevents(id,username,eventname,tag,timestart,timeend,description,rec,untildate,semester) values('" + eventId + "','"
      + Session["user"].ToString() + "','" + eventName.Text + "','" + typeDrop.SelectedValue + "','" + start.ToString() + "','" + end.ToString() + "','"
      + description.Text + "','" + recurrence + "','" + until.ToString() + "','" + semester + "');";
    
          cmd = new MySqlCommand(query2, conn);
          reader = cmd.ExecuteReader();
          reader.Close();
          start = tempDate;
          end = tempendDate;
        }
        if (CheckTues.Checked)
        {
            isTuesday = "true";
            DateTime tempendDate = end;
            DateTime tempDate = start;
            switch (start.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    start = start.AddDays(1);
                    end = end.AddDays(1);
                    break;
                case DayOfWeek.Tuesday:
                    break;
                case DayOfWeek.Wednesday:
                    start = start.AddDays(6);
                    end = end.AddDays(6);
                    break;
                case DayOfWeek.Thursday:
                    start = start.AddDays(5);
                    end = end.AddDays(5);
                    break;
                case DayOfWeek.Friday:
                    start = start.AddDays(4);
                    end = end.AddDays(4);
                    break;
                case DayOfWeek.Saturday:
                    start = start.AddDays(3);
                    end = end.AddDays(3);
                    break;
                case DayOfWeek.Sunday:
                    start = start.AddDays(2);
                    end = end.AddDays(2);
                    break;
            }
            id += 1;
            eventId = id.ToString();
            recurrence = RecurrenceRule.FromDateTime(id.ToString(), start).Weekly().Until(until).Encode();
            String query2 = "INSERT INTO userevents(id,username,eventname,tag,timestart,timeend,description,rec,untildate,semester) values('" + eventId + "','"
      + Session["user"].ToString() + "','" + eventName.Text + "','" + typeDrop.SelectedValue + "','" + start.ToString() + "','" + end.ToString() + "','"
      + description.Text + "','" + recurrence + "','" + until.ToString() + "','" + semester + "');";
            cmd = new MySqlCommand(query2, conn);
            reader = cmd.ExecuteReader();
            reader.Close();
            start = tempDate;
            end = tempendDate;
        }
        if (CheckWed.Checked)
        {
            isWednesday = "true";
            DateTime tempendDate = end;
            DateTime tempDate = start;
            switch (start.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    start = start.AddDays(2);
                    end = end.AddDays(2);
                    break;
                case DayOfWeek.Tuesday:
                    start = start.AddDays(1);
                    end = end.AddDays(1);
                    break;
                case DayOfWeek.Wednesday:
                    break;
                case DayOfWeek.Thursday:
                    start = start.AddDays(6);
                    end = end.AddDays(6);
                    break;
                case DayOfWeek.Friday:
                    start = start.AddDays(5);
                    end = end.AddDays(5);
                    break;
                case DayOfWeek.Saturday:
                    start = start.AddDays(4);
                    end = end.AddDays(4);
                    break;
                case DayOfWeek.Sunday:
                    start = start.AddDays(3);
                    end = end.AddDays(3);
                    break;
            }


            id += 1;
            eventId = id.ToString();
            recurrence = RecurrenceRule.FromDateTime(id.ToString(), start).Weekly().Until(until).Encode();
            String query2 = "INSERT INTO userevents(id,username,eventname,tag,timestart,timeend,description,rec,untildate,semester) values('" + eventId + "','"
       + Session["user"].ToString() + "','" + eventName.Text + "','" + typeDrop.SelectedValue + "','" + start.ToString() + "','" + end.ToString() + "','"
       + description.Text + "','" + recurrence + "','" + until.ToString() + "','" + semester + "');";
            cmd = new MySqlCommand(query2, conn);
            reader = cmd.ExecuteReader();
            reader.Close();
            start = tempDate;
            end = tempendDate;
        }
       if (CheckThurs.Checked)
        {
            isThursday = "true";
            DateTime tempendDate = end;
            DateTime tempDate = start;
            switch (start.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    start = start.AddDays(3);
                    end = end.AddDays(3);
                    break;
                case DayOfWeek.Tuesday:
                    start = start.AddDays(2);
                    end = end.AddDays(2);
                    break;
                case DayOfWeek.Wednesday:
                    start = start.AddDays(1);
                    end = end.AddDays(1);
                    break;
                case DayOfWeek.Thursday:
                    break;
                case DayOfWeek.Friday:
                    start = start.AddDays(6);
                    end = end.AddDays(6);
                    break;
                case DayOfWeek.Saturday:
                    start = start.AddDays(5);
                    end = end.AddDays(5);
                    break;
                case DayOfWeek.Sunday:
                    start = start.AddDays(4);
                    end = end.AddDays(4);
                    break;
            }
            id += 1;
            eventId = id.ToString();
            recurrence = RecurrenceRule.FromDateTime(id.ToString(), start).Weekly().Until(until).Encode();
            String query2 = "INSERT INTO userevents(id,username,eventname,tag,timestart,timeend,description,rec,untildate,semester) values('" + eventId + "','"
         + Session["user"].ToString() + "','" + eventName.Text + "','" + typeDrop.SelectedValue + "','" + start.ToString() + "','" + end.ToString() + "','"
         + description.Text + "','" + recurrence + "','" + until.ToString() + "','" + semester + "');";
            cmd = new MySqlCommand(query2, conn);
            reader = cmd.ExecuteReader();
            reader.Close();
            start = tempDate;
            end = tempendDate;
       }
       if (CheckFri.Checked)
       {
           isFriday = "true";
           DateTime tempendDate = end;
           DateTime tempDate = start;
           switch (start.DayOfWeek)
           {
               case DayOfWeek.Monday:
                   start = start.AddDays(4);
                   end = end.AddDays(4);
                   break;
               case DayOfWeek.Tuesday:
                   start = start.AddDays(3);
                   end = end.AddDays(3);
                   break;
               case DayOfWeek.Wednesday:
                   start = start.AddDays(2);
                   end = end.AddDays(2);
                   break;
               case DayOfWeek.Thursday:
                   start = start.AddDays(1);
                   end = end.AddDays(1);
                   break;
               case DayOfWeek.Friday:
                   break;
               case DayOfWeek.Saturday:
                   start = start.AddDays(6);
                   end = end.AddDays(6);
                   break;
               case DayOfWeek.Sunday:
                   start = start.AddDays(5);
                   end = end.AddDays(5);
                   break;
           }
           id += 1;
           eventId = id.ToString();
           recurrence = RecurrenceRule.FromDateTime(id.ToString(), start).Weekly().Until(until).Encode();
           String query2 = "INSERT INTO userevents(id,username,eventname,tag,timestart,timeend,description,rec,untildate,semester) values('" + eventId + "','"
        + Session["user"].ToString() + "','" + eventName.Text + "','" + typeDrop.SelectedValue + "','" + start.ToString() + "','" + end.ToString() + "','"
        + description.Text + "','" + recurrence + "','" + until.ToString() + "','" + semester + "');";
           cmd = new MySqlCommand(query2, conn);
           reader = cmd.ExecuteReader();
           reader.Close();
           start = tempDate;
           end = tempendDate;
        }
        if (CheckSat.Checked)
        {
            isSaturday = "true";
            DateTime tempDate = start;
            DateTime tempendDate = end;
            switch (start.DayOfWeek)
            {
                case DayOfWeek.Monday:
                  start = start.AddDays(5);
                  end = end.AddDays(5);
                    break;
                case DayOfWeek.Tuesday:
                    start = start.AddDays(4);
                    end = end.AddDays(4);
                    break;
                case DayOfWeek.Wednesday:
                    start = start.AddDays(3);
                    end = end.AddDays(3);
                    break;
                case DayOfWeek.Thursday:
                    start = start.AddDays(2);
                    end = end.AddDays(2);
                    break;
                case DayOfWeek.Friday:
                   start = start.AddDays(1);
                   end = end.AddDays(1);
                    break;
                case DayOfWeek.Saturday:
                    break;
                case DayOfWeek.Sunday:
                    end = end.AddDays(6);
                   start = start.AddDays(6);
                    break;
            }
            id += 1;
            eventId = id.ToString();
            recurrence = RecurrenceRule.FromDateTime(id.ToString(), start).Weekly().Until(until).Encode();
          String query2 = "INSERT INTO userevents(id,username,eventname,tag,timestart,timeend,description,rec,untildate,semester) values('" + eventId + "','"
         + Session["user"].ToString() + "','" + eventName.Text + "','" + typeDrop.SelectedValue + "','" + start.ToString() + "','" + end.ToString() + "','"
         + description.Text + "','" + recurrence + "','" + until.ToString() + "','" + semester + "');";
            cmd = new MySqlCommand(query2, conn);
            reader = cmd.ExecuteReader();
            reader.Close();
            start = tempDate;
            end = tempendDate;
        }
       if (CheckSun.Checked)
        {
            isSunday = "true";
            DateTime tempendDate = end;
            DateTime tempDate = start;
            switch (start.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    start = start.AddDays(6);
                    end = end.AddDays(6);
                    break;
                case DayOfWeek.Tuesday:
                    start = start.AddDays(5);
                    end = end.AddDays(5);
                    break;
                case DayOfWeek.Wednesday:
                    start = start.AddDays(4);
                    end = end.AddDays(4);
                    break;
                case DayOfWeek.Thursday:
                    start = start.AddDays(3);
                    end = end.AddDays(3);
                    break;
                case DayOfWeek.Friday:
                    start = start.AddDays(2);
                    end = end.AddDays(2);
                    break;
                case DayOfWeek.Saturday:
                    start = start.AddDays(1);
                    end = end.AddDays(1);
                    break;
                case DayOfWeek.Sunday:
                    break;
            }
            id += 1;
            eventId = id.ToString();
            recurrence = RecurrenceRule.FromDateTime(id.ToString(), start).Weekly().Until(until).Encode();
            String query2 = "INSERT INTO userevents(id,username,eventname,tag,timestart,timeend,description,rec,untildate,semester) values('" + eventId + "','"
         + Session["user"].ToString() + "','" + eventName.Text + "','" + typeDrop.SelectedValue + "','" + start.ToString() + "','" + end.ToString() + "','"
         + description.Text + "','" + recurrence + "','" + until.ToString() + "','" + semester + "');";
            cmd = new MySqlCommand(query2, conn);
            reader = cmd.ExecuteReader();
            reader.Close();
            start = tempDate;
            end = tempendDate;
     
       }

       String addClass = "INSERT INTO class(ClassName,Prof,OnMonday,OnTuesday,OnWednesday,OnThursday,OnFriday, OnSaturday, OnSunday, Semester, user) values('"
           + ClassName + "','" + prof + "','" + isMonday + "','" + isTuesday + "','" + isWednesday + "','" + isThursday + "','" + isFriday + "','" + isSaturday + "','" + isSunday + "','" + semester + "','" + user + "');";
       cmd = new MySqlCommand(addClass, conn);
       reader = cmd.ExecuteReader();
       reader.Close();

       conn.Close();
       Response.Redirect("MainView.aspx");
       
    }


    public void ASPNET_MsgBox(String message)
    {
        System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">" + System.Environment.NewLine);

        System.Web.HttpContext.Current.Response.Write("alert(\"" + message + "\")" + System.Environment.NewLine);

        System.Web.HttpContext.Current.Response.Write("</SCRIPT>");
    }
    protected void typeDrop_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void HomeButton2_Click(object sender, EventArgs e)
    {
        Response.Redirect("MainView.aspx");
    }
}