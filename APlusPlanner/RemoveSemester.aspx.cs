﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using MySql.Data;

public partial class Professor : System.Web.UI.Page
{
    public MySqlConnection conn;
    public MySqlDataReader reader;
    public MySqlDataAdapter adapter;

    public String redirect;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        SemesterDropDown.Items.Clear();
        if (Session["LoggedIn"] == null || Session["LoggedIn"] != "1")
        {
            Response.Redirect("LoginView.aspx");
        }
        conn = new MySqlConnection();
        conn.ConnectionString = "server=70.75.191.26; user id=root; password=; database=aplusplus;Allow Zero Datetime=True";
        conn.Open();

        String query = "select * from semester where username='" + Session["user"].ToString() + "';";

        MySqlCommand cmd = new MySqlCommand(query, conn);
        reader = cmd.ExecuteReader();

        while (reader.Read())
        {
            SemesterDropDown.Items.Add(new ListItem(reader.GetString(reader.GetOrdinal("name")), reader.GetString(reader.GetOrdinal("name"))));
        }

        reader.Close();

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        MySqlCommand cmd;

        conn = new MySqlConnection();
        conn.ConnectionString = "server=70.75.191.26; user id=root; password=; database=aplusplus;Allow Zero Datetime=True";
        conn.Open();

        String query = "select * from semester where username='" + Session["user"].ToString() + "' and name='" + SemesterDropDown.SelectedValue.ToString() + "';";
        cmd = new MySqlCommand(query, conn);
        reader = cmd.ExecuteReader();
        String names = "";
        String start = "";
        String end = "";

        while (reader.Read())
        {
            names = "";
            start = "";
            end = "";
            names = reader.GetString(reader.GetOrdinal("name"));
            start = start + reader.GetString(reader.GetOrdinal("startyear")) + "-" + reader.GetString(reader.GetOrdinal("startmonth")) + "-" + reader.GetString(reader.GetOrdinal("startday"));
            end = end + reader.GetString(reader.GetOrdinal("endyear")) + "-" + reader.GetString(reader.GetOrdinal("endmonth")) + "-" + reader.GetString(reader.GetOrdinal("endday"));
        }
        name.Text = names;
        startDate.Text = start;
        endDate.Text = end;
        reader.Close();

        conn.Close();
    }
    protected void semList_TextChanged(object sender, EventArgs e)
    {

    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        MySqlCommand cmd;

        conn = new MySqlConnection();
        conn.ConnectionString = "server=70.75.191.26; user id=root; password=; database=aplusplus;Allow Zero Datetime=True";
        conn.Open();
        String query = "DELETE from semester WHERE name='" + name.Text + "' and username='" + Session["user"].ToString() + "';";


        cmd = new MySqlCommand(query, conn);
        reader = cmd.ExecuteReader();
        reader.Close();

        String query2 = "DELETE from userevents WHERE semester='" + name.Text + "';";
        cmd = new MySqlCommand(query2, conn);
        reader = cmd.ExecuteReader();
        reader.Close();

        String query3 = "DELETE from class WHERE Semester = '" + name.Text + "' AND user = '" + Session["user"].ToString() + "';";
        cmd = new MySqlCommand(query3, conn);
        reader = cmd.ExecuteReader();
        reader.Close();

        conn.Close();
        Response.Redirect("MainView.aspx");
    }
    protected void HomeButton1_Click(object sender, EventArgs e)
    {
        Response.Redirect("MainView.aspx");
    }
    protected void SemesterDropDown_SelectedIndexChanged(object sender, EventArgs e)
    {
        name.Text = "Test";
    }
}