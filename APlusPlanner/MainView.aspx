﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MainView.aspx.cs" Inherits="MainView" %>

<%@ Register Assembly="DayPilot" Namespace="DayPilot.Web.Ui" TagPrefix="DayPilot" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .inlineBlock {
        }
    </style>
    <link href="MainView.css" rel="stylesheet" type="text/css" />

</head>
<body style="height: 829px">
    <form id="form1" runat="server">
        <div style="height: 728px; margin-left: 120px; width: 1734px;">
            <asp:Panel ID="Panel5" runat="server" Height="55px">
                <asp:Panel ID="Panel3" runat="server" Height="55px">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="MS Reference Sans Serif" Font-Size="XX-Large" Text="A+ Student Planner - Web"></asp:Label>
                   
                    <br />
                </asp:Panel>
                <asp:Panel ID="Panel4" runat="server" Height="23px" Width="1400px" Style="float: left;">
                     <asp:LinkButton ID="LinkButton1" runat="server" Style="margin-left:0px;" OnClick="LinkButton1_Click" Font-Names="Microsoft Sans Serif" Font-Underline="False" ForeColor="Black">Logout</asp:LinkButton>
                    <br />
                </asp:Panel>
                <asp:Panel ID="Panel8" runat="server" Height="23px" Style="float: left;">
                    <hr style="width: 1600px" />
                    <br />
                </asp:Panel>
                <div style="height: 151px; margin-left: 0px; float: left; width: 99%;">
                    <asp:Panel ID="Panel2" runat="server">

                        <div style="height: 100px; width: 100px; margin-left: 0px; float: left; margin-right: 50px;">

                            <div class="dropdown" id="schedule" style="display: inline;">
                              <%--  <a class="account">Schedule</a>--%>
                                <asp:Label ID="Label3" runat="server" Text="Schedule" Font-Size="Large" Font-Bold="True" Font-Italic="False" Font-Underline="False"></asp:Label>
                                  <br />
<%--                                <div class="submenu">
                                    <ul class="root">--%>
                                        <asp:LinkButton ID="Button1" runat="server" Width="90px" Text="View" BackColor="White" BorderStyle="None" Font-Size="Medium" Font-Names="Microsoft Sans Serif" Font-Underline="False" ForeColor="Black" OnClick="Button1_Click" />
                                <br />        
                                <asp:LinkButton ID="Button2" runat="server" BackColor="White" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Font-Underline="False" ForeColor="Black" OnClick="Button2_Click" Text="Add Event" Width="90px" />
                                <br />       
                                <br />       
                                        <%--                                        <li><a href="#viewSched">View Schedule</a></li>
                                        <li><a href="#addEvent">Add Event</a></li>
                                        <li><a href="#editEvent">Edit Event</a></li>
                                        <li><a href="#delEvent">Remove Event</a></li>--%>
<%--                                    </ul>
                                </div>--%>
                            </div>
                        </div>

                        <div style="height: 728px; width: 200px; margin-left: 50px; float: left;">
                            <div class="dropdown" id="professor" style="display: inline;">
                            <%--    <a class="account">Professor</a>--%>
                                 <asp:Label ID="Label4" runat="server" Text="Professor" Font-Size="Large" Font-Bold="False"></asp:Label>
                                <br />
<%--                                <div class="submenu">
                                    <ul class="root">--%>
                                        <asp:LinkButton ID="Button4" runat="server" Text="View Professor" BackColor="White" BorderStyle="None" Font-Size="Medium" Font-Names="Microsoft Sans Serif" Font-Underline="False" ForeColor="Black" OnClick="Button4_Click" />
                                <br />         
                                <asp:LinkButton ID="Button5" runat="server" Text="Add Professor" BackColor="White" BorderStyle="None" Font-Size="Medium" Font-Names="Microsoft Sans Serif" Font-Underline="False" ForeColor="Black" OnClick="Button5_Click" />
                                         <br /> 
                                 <asp:LinkButton ID="Button6" runat="server" BackColor="White" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Font-Underline="False" ForeColor="Black" OnClick="Button6_Click" Text="Edit Professor" />
                             
                                 <br />
                                 <asp:LinkButton ID="Button7" runat="server" Text="Remove Professor" BackColor="White" BorderStyle="None" Font-Size="Medium" Font-Names="Microsoft Sans Serif" Font-Underline="False" ForeColor="Black" OnClick="Button7_Click" />
                                        <%--<li><a href="#viewProf">View Professor</a></li>
                                        <li><a href="#addProf">Add Professor</a></li>
                                        <li><a href="#editProf">Edit Professor</a></li>
                                        <li><a href="#delProf">Remove Professor</a></li>--%>
<%--                                    </ul>
                                </div>--%>
                            </div>
                        </div>
                        <div style="height: 728px; width: 200px; margin-left: 50px; float: left;" >
                            <div class="dropdown" id="class" style="display: inline;">
                             <%--   <a class="account">Class</a>--%>
                                 <asp:Label ID="Label5" runat="server" Text="Class" Font-Size="Large" Font-Bold="False"></asp:Label>
                                <br />
<%--                                <div class="submenu">
                                    <ul class="root">--%>
        
                                        <asp:LinkButton ID="Button8" runat="server" BackColor="White" BorderStyle="None" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Font-Underline="False" ForeColor="Black" OnClick="Button8_Click" style="height: 20px" Text="View Class" />
                                 <br />       
                                 <asp:LinkButton ID="Button9" runat="server" Text="Add Class" BackColor="White" BorderStyle="None" Font-Size="Medium" Font-Names="Microsoft Sans Serif" Font-Underline="False" ForeColor="Black" OnClick="Button9_Click" />
                                <br />
                                        <asp:LinkButton ID="Button10" runat="server" Text="Edit Class" BackColor="White" BorderStyle="None" Font-Size="Medium" Font-Names="Microsoft Sans Serif" Font-Underline="False" ForeColor="Black" OnClick="Button10_Click" />
                                <br />
                                        <asp:LinkButton ID="Button11" runat="server" Text="Remove Class" BackColor="White" BorderStyle="None" Font-Size="Medium" Font-Names="Microsoft Sans Serif" Font-Underline="False" ForeColor="Black" OnClick="Button11_Click" />
                                        <%--                                        <li><a href="#viewTask">View Class</a></li>
                                        <li><a href="#addTask">Add Class</a></li>
                                        <li><a href="#editTask">Edit Class</a></li>
                                        <li><a href="#delClass">Remove Class</a></li>--%>
<%--                                    </ul>
                                </div>--%>
                            </div>
                        </div>
                        <div style="height: 728px; width: 200px; margin-left: 50px; float: left;">
                            <div class="dropdown" id="semester" style="display: inline;">
                              <%--  <a class="account">Semester</a>--%>
                                 <asp:Label ID="Label6" runat="server" Text="Semester" Font-Size="Large" Font-Bold="False"></asp:Label>
                                <br />
<%--                                <div class="submenu">
                                    <ul class="root">--%>
                                        <asp:LinkButton ID="Button12" runat="server" Text="View Semester" BackColor="White" BorderStyle="None" Font-Size="Medium" Font-Names="Microsoft Sans Serif" Font-Underline="False" ForeColor="Black" OnClick="Button12_Click" />
                                <br />         
                                <asp:LinkButton ID="Button13" runat="server" Text="Add Semester" BackColor="White" BorderStyle="None" Font-Size="Medium" Font-Names="Microsoft Sans Serif" Font-Underline="False" ForeColor="Black" OnClick="Button13_Click" />
                                <br /> 
                                <asp:LinkButton ID="Button15" runat="server" Text="Remove Semester" BackColor="White" BorderStyle="None" Font-Size="Medium" Font-Names="Microsoft Sans Serif" Font-Underline="False" ForeColor="Black" OnClick="Button15_Click" />

                                        <%--                                        <li><a href="#viewSemester">View Semester</a></li>
                                        <li><a href="#addSemester">Add Semester</a></li>
                                        <li><a href="#editSemester">Edit Semester</a></li>
                                        <li><a href="#delSemester">Remove Semester</a></li>--%>
<%--                                    </ul>
                                </div>--%>
                            </div>
                        </div>
                        <div style="height: 728px; width: 200px; margin-left: 50px; float: left;">
                            <div class="dropdown" id="other">
                               <%-- <a class="account">Other</a>--%>
                                 <asp:Label ID="Label7" runat="server" Text="Other" Font-Size="Large" Font-Bold="False"></asp:Label>
                                <br />
<%--                                <div class="submenu">
                                    <ul class="root">--%>
                                        <asp:LinkButton ID="Button16" runat="server" Text="About" BackColor="White" BorderStyle="None" Font-Size="Medium" Font-Names="Microsoft Sans Serif" Font-Underline="False" ForeColor="Black" OnClick="Button16_Click" />
                                <br />
                                        <asp:LinkButton ID="Button17" runat="server" Text="Help" BackColor="White" BorderStyle="None" Font-Size="Medium" Font-Names="Microsoft Sans Serif" Font-Underline="False" ForeColor="Black" OnClick="Button17_Click" />
                                 <br />       
                                 <asp:LinkButton ID="Button19" runat="server" Text="Feedback" BackColor="White" BorderStyle="None" Font-Size="Medium" Font-Names="Microsoft Sans Serif" Font-Underline="False" ForeColor="Black" OnClick="Button19_Click" />
                                        <%--                                        <li><a href="#about">About</a></li>
                                        <li><a href="#help">Help</a></li>
                                        <li><a href="#settings">Settings</a></li>
                                        <li><a href="#feedback">Send Feedback</a></li>--%>
<%--                                    </ul>
                                </div>--%>
                            </div>
                        </div>
                        <br />
                    </asp:Panel>
                </div>

            </asp:Panel>
            <asp:Panel ID="Panel7" runat="server" Height="130px" Width="1393px">
                <br />
 
                <br />
            </asp:Panel>
            <asp:Panel ID="Panel1" runat="server" Height="667px" Width="1291px">

                <div style="height: 2px; margin-left: 0px; float: left; width: 25%;">
                    <asp:Calendar ID="calDate" runat="server"
    BackColor="White" BorderColor="#3366CC"  CellPadding="1" DayNameFormat="Shortest"
    Font-Names="Verdana" Font-Size="8pt" ForeColor="#003399" Height="180px" Width="320px" OnSelectionChanged="calDate_SelectionChanged1">
    <SelectedDayStyle BackColor="#009999" Font-Bold="True" ForeColor="#CCFF99" />
    <TodayDayStyle BackColor="#99CCCC" ForeColor="White" />
    <SelectorStyle BackColor="#99CCCC" ForeColor="#336666" />
    <WeekendDayStyle BackColor="#CCCCFF" />
    <OtherMonthDayStyle ForeColor="#999999" />
    <NextPrevStyle Font-Size="8pt" ForeColor="#CCCCFF" />
    <DayHeaderStyle BackColor="#99CCCC" ForeColor="#336666" Height="1px" />
    <TitleStyle BackColor="#003399" BorderColor="#3366CC" 
        BorderWidth="1px" Font-Bold="True"
        Font-Size="10pt" ForeColor="#CCCCFF" Height="25px" />
</asp:Calendar>

                   
                    <asp:DropDownList ID="DropDownList1" runat="server" Height="23px" Style="float: right; margin-right:10px; margin-top:10px" ViewStateMode="Disabled" Width="99px" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged1">
                        <asp:ListItem Value="0"># days</asp:ListItem>
                        <asp:ListItem Value="1">1 day</asp:ListItem>
                        <asp:ListItem Value="3">3 day</asp:ListItem>
                        <asp:ListItem Value="5">5 day</asp:ListItem>
                        <asp:ListItem Value="7">7 day</asp:ListItem>
                    </asp:DropDownList>
                    <br />
                
                    <br />
                <br />
                     <asp:Label ID="Label8" runat="server" Text="Name:" Font-Size="Large" Width="100px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:TextBox ID="eventName" runat="server"  Font-Size="Large" Width="188px" Font-Names="Microsoft Sans Serif" ></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label9" runat="server" Text="Start Time:" Font-Size="Large" Width="100px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:TextBox ID="eventStartHour" runat="server"  Font-Size="Large" Width="32px" Font-Names="Microsoft Sans Serif" TextMode="DateTime" PlaceHolder="HH"></asp:TextBox>
                    <asp:Label ID="Label12" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text=" : " Width="22px" Style="margin-left:10px;" ></asp:Label>
                    <asp:TextBox ID="eventStartMinute" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" PlaceHolder="MM"></asp:TextBox>
                <br />
                <asp:Label ID="Label10" runat="server" Text="End Time:"  Font-Size="Large" Width="100px" Font-Names="Microsoft Sans Serif"></asp:Label>
                    <asp:TextBox ID="eventEndHour" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" PlaceHolder="HH"></asp:TextBox>
                    <asp:Label ID="Label13" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Style="margin-left:10px;" Text=" : " Width="22px"></asp:Label>
                    <asp:TextBox ID="eventEndMinute" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" PlaceHolder="MM"></asp:TextBox>
                    <br />
                <br />
                  
                  <asp:Label ID="Label2" runat="server" Text="Start Date:" Font-Size="Large" Width="99px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:TextBox ID="startYear" runat="server"  Font-Size="Large" Width="60px" Font-Names="Microsoft Sans Serif" TextMode="DateTime" placeholder="yyyy" ></asp:TextBox>
                    <asp:Label ID="Label15" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="-" Width="16px" Style="margin-left:10px;"></asp:Label>
                    <asp:TextBox ID="startMonth" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" placeholder="mm"></asp:TextBox>
                 <asp:Label ID="Label16" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="-" Width="16px" Style="margin-left:10px;"></asp:Label>
                    <asp:TextBox ID="startDay" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" placeholder="dd"></asp:TextBox>
                <br />
                <asp:Label ID="Label18" runat="server" Text="End Date:"  Font-Size="Large" Width="99px" Font-Names="Microsoft Sans Serif"></asp:Label>
                    <asp:TextBox ID="endYear" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="60px" placeholder="yyyy"></asp:TextBox>
                    <asp:Label ID="Label19" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Style="margin-left:10px;" Text="-" Width="16px"></asp:Label>
                    <asp:TextBox ID="endMonth" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" placeholder="mm"></asp:TextBox>
                <asp:Label ID="Label20" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Style="margin-left: 10px;" Text="-" Width="16px"></asp:Label>
                <asp:TextBox ID="endDay" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" placeholder="dd"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label11" runat="server" Text="Description:"  Font-Size="Large" Width="100px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:TextBox ID="description" runat="server"  Font-Size="Large" Width="191px" TextMode="MultiLine" Font-Names="Microsoft Sans Serif" Height="68px" ></asp:TextBox>
                <br />
                     <br /> 
                        <asp:Label ID="Label14" runat="server" Text="Recurrence:"  Font-Size="Large" Width="100px" Font-Names="Microsoft Sans Serif"></asp:Label>
                    <asp:DropDownList ID="recDrop" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Style="margin-left:0px;" Width="193px">
                        <asp:ListItem Value="0">no recurrence</asp:ListItem>
                        <asp:ListItem Value="daily">daily</asp:ListItem>
                        <asp:ListItem>weekly</asp:ListItem>
                        <asp:ListItem>monthly</asp:ListItem>
                    </asp:DropDownList>  <br />
                     <br />
                     <asp:Button ID="Button21" runat="server" Text="Add Event" style="margin-left:100px;" Font-Names="Microsoft Sans Serif" Font-Size="Large" Width="100px" OnClick="Button21_Click"  />
                   <br />
                </div>
                <div style="height: 653px; margin-left: 0px; float: right; width: 75%;">
                    <DayPilot:DayPilotCalendar ID="DayView" runat="server" Days="5" Width="1280px" style="top: 0px; left: 0px; height: 600px" CellHeight="16" BusinessBeginsHour="4" BusinessEndsHour="23" ContextMenuID="DayPilotMenu1" EventClickHandling="ContextMenu" OnEventMenuClick="DayView_EventMenuClick1" />
                </div>
            </asp:Panel>
            <asp:Panel ID="Panel6" runat="server" Height="18px">
                 <DayPilot:DayPilotMenu ID="DayPilotMenu1" runat="server">
                        <DayPilot:MenuItem Text="Delete" Action="Postback" Command="Delete" />
                     <DayPilot:MenuItem Text="Edit" Action="Postback" Command="Edit" />
                    </DayPilot:DayPilotMenu>

                <br />
            </asp:Panel>
        </div>
    </form>


</body>

</html>


