﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data;
using MySql.Data.MySqlClient;

public partial class LoginView : System.Web.UI.Page
{
    public MySqlConnection conn;
    public MySqlDataReader reader;
    public MySqlDataAdapter adapter;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoggedIn"] == null) {
            Session["LoggedIn"] = "0";
        }
        else if (Session["LoggedIn"] == "1") {
            Response.Redirect("MainView.aspx");
        }

        conn = new MySqlConnection();
        conn.ConnectionString = "server=70.75.191.26; user id=root; password=; database=aplusplus";
        conn.Open();
        loginFail.Visible = false;
    }

    public void ASPNET_MsgBox(String message)
    {
        System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">" + System.Environment.NewLine);

        System.Web.HttpContext.Current.Response.Write("alert(\"" + message + "\")" + System.Environment.NewLine);

        System.Web.HttpContext.Current.Response.Write("</SCRIPT>");
    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        String query;
        String user = userInput.Text;
        String pass = passInput.Text;
        query = "select * from users where username='" + user + "' and password='" + pass + "';";

        MySqlCommand cmd = new MySqlCommand(query, conn);
        reader = cmd.ExecuteReader();
        int count = 0;
        if (reader.Read())
        {
            ASPNET_MsgBox("Success");
            Session["user"] = user;
            Session["password"] = pass;
            Session["LoggedIn"] = "1";
            loginFail.Visible = false;
            conn.Close();
            Response.Redirect("MainView.aspx");
        }
        else
        {
            ASPNET_MsgBox("Fail");
            loginFail.Visible = true;
        }
    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        conn.Close();
        Response.Redirect("RegistrationView.aspx");
    }
}