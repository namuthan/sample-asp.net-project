﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using MySql.Data;

public partial class Professor : System.Web.UI.Page
{
    public MySqlConnection conn;
    public MySqlDataReader reader;
    public MySqlDataAdapter adapter;

    public String redirect;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoggedIn"] == null || Session["LoggedIn"] != "1")
        {
            Response.Redirect("LoginView.aspx");
        }
        conn = new MySqlConnection();
        conn.ConnectionString = "server=70.75.191.26; user id=root; password=; database=aplusplus;Allow Zero Datetime=True";
        conn.Open();

        String query;
        query = "select * from professor where username='" + Session["user"].ToString()+"';";

        MySqlCommand cmd = new MySqlCommand(query, conn);
        reader = cmd.ExecuteReader();
        String input = "";
        String lastname = "";

        while (reader.Read())
        {
            input = reader.GetString(reader.GetOrdinal("title")) + " " + reader.GetString(reader.GetOrdinal("firstname")) + " " + reader.GetString(reader.GetOrdinal("lastname"));
            lastname = reader.GetString(reader.GetOrdinal("lastname"));
            ProfessorDropDown.Items.Add(new ListItem(input, lastname));
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        MySqlCommand cmd;

        conn = new MySqlConnection();
        conn.ConnectionString = "server=70.75.191.26; user id=root; password=; database=aplusplus;Allow Zero Datetime=True";
        conn.Open();

        String lastnamein = ProfessorDropDown.SelectedValue.ToString();

        String query = "select * from professor where username='" + Session["user"].ToString() + "' and lastname='" + lastnamein + "';";
        cmd = new MySqlCommand(query, conn);
        reader = cmd.ExecuteReader();
        String title = "";
        String firstname = "";
        String lastname = "";
        String email = "";
        String officelocation = "";
        String phonenumber = "";
        String officedays = "";
        String officehours = "";

        while (reader.Read())
        {
            title = reader.GetString(reader.GetOrdinal("title"));
            firstname = reader.GetString(reader.GetOrdinal("firstname"));
            lastname = reader.GetString(reader.GetOrdinal("lastname"));
            email = reader.GetString(reader.GetOrdinal("email"));
            officelocation = reader.GetString(reader.GetOrdinal("officelocation"));
            phonenumber = reader.GetString(reader.GetOrdinal("phonenumber"));
            officedays = reader.GetString(reader.GetOrdinal("officedays"));
            officehours = reader.GetString(reader.GetOrdinal("officehours"));
        }
        proftitle.Text = title;
        proffirst.Text = firstname;
        proflast.Text = lastname;
        profemail.Text = email;
        proflocation.Text = officelocation;
        profphone.Text = phonenumber;
        profdays.Text = officedays;
        profhours.Text = officehours;

        conn.Close();
    }
    protected void HomeButton1_Click(object sender, EventArgs e)
    {
        Response.Redirect("MainView.aspx");
    }
}