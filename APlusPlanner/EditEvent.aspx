﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditEvent.aspx.cs" Inherits="EditEvent" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
     <form id="form1" runat="server">
        <div style="height: 728px; margin-left: 120px; width: 1734px;">
            <asp:Panel ID="Panel5" runat="server" Height="55px">
                <asp:Panel ID="Panel3" runat="server" Height="55px">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="MS Reference Sans Serif" Font-Size="XX-Large" Text="A+ Student Planner - Web"></asp:Label>
                    <br />
                </asp:Panel>
                <asp:Panel ID="Panel4" runat="server" Height="16px" Style="float: left;">
                    <hr style="width: 800px; height: -12px;" />
                    <br />
                       <br />
                </asp:Panel>
             <br />
                <asp:Panel ID="Panel10" runat="server" Height="42px" Style="float: left;" Width="200px">
                        <br />
                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Names="MS Reference Sans Serif" Font-Size="Large" Text="Edit Event"></asp:Label>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="Panel7" runat="server" Height="89px" Width="1393px">
                <br />
                <br />
                <br />
            </asp:Panel>
            <asp:Panel ID="Panel1" runat="server" Height="533px" Width="1291px"  style="margin-left:80px;">
                <%--login--%>
                <asp:Label ID="Label5" runat="server" Text="Event Name:" Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:TextBox ID="eventName" runat="server"  Font-Size="Large" Width="560px" Font-Names="Microsoft Sans Serif" ></asp:TextBox>
                <br />
                    <br />
                <asp:Label ID="Label6" runat="server" Text="Event Type:"  Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:DropDownList ID="eventType" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Style="margin-left:0px;" Width="141px">
                    <asp:ListItem Value="0">type</asp:ListItem>
                    <asp:ListItem Value="lecture">lecture</asp:ListItem>
                    <asp:ListItem>tutorial</asp:ListItem>
                    <asp:ListItem>lab</asp:ListItem>
                    <asp:ListItem>event</asp:ListItem>
                </asp:DropDownList>
                <br />
                <br />
                <asp:Label ID="Label8" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="Start Time:" Width="150px"></asp:Label>
                <asp:TextBox ID="starthour" runat="server"  Font-Size="Large" Width="32px" Font-Names="Microsoft Sans Serif" TextMode="DateTime" ></asp:TextBox>
                <asp:Label ID="Label11" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Style="margin-left:10px;" Text=" : " Width="22px"></asp:Label>
                <asp:TextBox ID="startminute" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px"></asp:TextBox>
                <br />
                    <br />
                <asp:Label ID="Label14" runat="server" Text="End Time:" Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:TextBox ID="endhour" runat="server"  Font-Size="Large" Width="32px" Font-Names="Microsoft Sans Serif" TextMode="DateTime"></asp:TextBox>
                    <asp:Label ID="Label15" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text=" : " Width="22px" Style="margin-left:10px;"></asp:Label>
                    <asp:TextBox ID="endminute" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px"></asp:TextBox>
                 <br />
                <br />
                <asp:Label ID="Label16" runat="server" Text="Start Date:"  Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                    <asp:TextBox ID="startyear" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="80px" placeholder="yyyy"></asp:TextBox>
                    <asp:Label ID="Label17" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Style="margin-left:10px;" Text="-" Width="22px"></asp:Label>
                    <asp:TextBox ID="startmonth" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" placeholder="mm"></asp:TextBox>
                <asp:Label ID="Label21" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Style="margin-left:10px;" Text="-" Width="22px"></asp:Label>
                <asp:TextBox ID="startday" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" placeholder="dd"></asp:TextBox>
                <br />
                <br />
                 <asp:Label ID="Label18" runat="server" Text="End Date:" Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:TextBox ID="endyear" runat="server"  Font-Size="Large" Width="80px" Font-Names="Microsoft Sans Serif" TextMode="DateTime" placeholder="yyyy" ></asp:TextBox>
                    <asp:Label ID="Label19" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="-" Width="22px" Style="margin-left:10px;"></asp:Label>
                    <asp:TextBox ID="endmonth" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" placeholder="mm"></asp:TextBox>
                <asp:Label ID="Label20" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Style="margin-left:10px;" Text="-" Width="22px"></asp:Label>
                    <asp:TextBox ID="endday" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" placeholder="dd"></asp:TextBox>
                 <br />
                <br />
                <asp:Label ID="Label3" runat="server" Text="Description:"  Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                    <asp:TextBox ID="description" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="MultiLine" Width="560px" Height="151px"></asp:TextBox>
                    <br />
                <br />
                    <asp:Label ID="Label4" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="Recurrence:" Width="150px"></asp:Label>
                    <asp:DropDownList ID="recDrop" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Style="margin-left:0px;" Width="141px">
                        <asp:ListItem Value="0">recurrence</asp:ListItem>
                        <asp:ListItem Value="daily">daily</asp:ListItem>
                        <asp:ListItem>weekly</asp:ListItem>
                        <asp:ListItem>monthly</asp:ListItem>
                </asp:DropDownList>
                <br />
                <br />
                    

            </asp:Panel>
             <asp:Panel ID="Panel6" runat="server" Height="70px" Width="1393px" style="margin-left:550px;">
                <asp:Button ID="Button1" runat="server" Text="Submit" style="margin-left:150px;" Font-Names="Microsoft Sans Serif" Font-Size="Large" Width="100px" OnClick="Button1_Click"  />
                   <br />
                  <asp:Label ID="loginFail" runat="server" Font-Names="Microsoft Sans Serif" style="margin-left:140px;" Text="Please try again" Visible="False"></asp:Label>

                <br />
            </asp:Panel>
                 <asp:Panel ID="Panel9" runat="server" Height="16px" Style="float:left;" Width="594px">
                    <hr style="width: 800px; float:left; margin-left:0px;"/>
                    <br />
                </asp:Panel>
             <asp:Panel ID="Panel11" runat="server" Height="16px" Style="float:left;" Width="986px">
                <br />
                    <br />
                </asp:Panel>
                         <asp:Panel ID="Panel8" runat="server" Height="50px" Width="1393px" style="margin-left:180px;">
                <br />
            </asp:Panel>
<br />

        </div>
    </form>
</body>
</html>
