﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using MySql.Data.MySqlClient;
using MySql.Data;
using DayPilot;
using DayPilot.Web.Ui.Recurrence;


public partial class MainView : System.Web.UI.Page
{

    public MySqlConnection conn;
    public MySqlDataReader reader;
    public MySqlDataAdapter adapter;

    public String redirect;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoggedIn"] == null || Session["LoggedIn"] != "1") {
            Response.Redirect("LoginView.aspx");
        }
        conn = new MySqlConnection();
        conn.ConnectionString = "server=70.75.191.26; user id=root; password=; database=aplusplus;Allow Zero Datetime=True";
        conn.Open();

        DataTable userEvents = new DataTable();

        String getEventsQuery = "SELECT * FROM userevents WHERE username='" + Session["user"] + "';";
        MySqlCommand cmd = new MySqlCommand(getEventsQuery, conn);
        userEvents.Load(cmd.ExecuteReader());

        if (userEvents.Rows.Count >= 1)
        {
            DayView.DataSource = userEvents;
            DayView.DataStartField = "timestart";
            DayView.DataEndField = "timeend";
            DayView.DataTextField = "eventname";
            DayView.DataIdField = "id";
            DayView.DataRecurrenceField = "rec";

            DataBind();
        }

        DataBind();
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Session["LoggedIn"] = 0;
        Session["user"] = null;
        Session["password"] = null;
        Response.Redirect("LoginView.aspx");
    }



    protected void DropDownList1_SelectedIndexChanged1(object sender, EventArgs e)
    {
        int days = Convert.ToInt32(DropDownList1.SelectedValue);
        DayView.Days = days;
    }
    protected void calDate_SelectionChanged1(object sender, EventArgs e)
    {
        DayView.StartDate = calDate.SelectedDate;
    }
    protected void Button21_Click(object sender, EventArgs e)
    {
        MySqlCommand cmd;

        String selectLast = "SELECT id FROM userevents ORDER BY ID DESC LIMIT 1;";
        cmd = new MySqlCommand(selectLast, conn);
        reader = cmd.ExecuteReader();
        reader.Read();
        int id = reader.GetInt32(0);
        id += 1;
        String eventId = id.ToString();
        reader.Close();

        conn = new MySqlConnection();
        conn.ConnectionString = "server=70.75.191.26; user id=root; password=; database=aplusplus";
        conn.Open();

        DateTime start = new DateTime(Int32.Parse(startYear.Text), Int32.Parse(startMonth.Text), Int32.Parse(startDay.Text), Int32.Parse(eventStartHour.Text), Int32.Parse(eventStartMinute.Text), 0);

        DateTime end = new DateTime(Int32.Parse(startYear.Text), Int32.Parse(startMonth.Text), Int32.Parse(startDay.Text), Int32.Parse(eventEndHour.Text), Int32.Parse(eventEndMinute.Text), 0);

        DateTime until = new DateTime(Int32.Parse(endYear.Text), Int32.Parse(endMonth.Text), Int32.Parse(endDay.Text), Int32.Parse(eventEndHour.Text), Int32.Parse(eventEndMinute.Text), 0);

        String recurrence;
        if (recDrop.SelectedValue == "daily")
        {
            recurrence = RecurrenceRule.FromDateTime(id.ToString(), start).Daily().Until(until).Encode();
        }
        else if (recDrop.SelectedValue == "weekly")
        {
            recurrence = RecurrenceRule.FromDateTime(id.ToString(), start).Weekly().Until(until).Encode();
        }
        else if (recDrop.SelectedValue == "monthly")
        {
            recurrence = RecurrenceRule.FromDateTime(id.ToString(), start).Monthly().Until(until).Encode();
        }
        else
        {
            recurrence = RecurrenceRule.FromDateTime(id.ToString(), start).Daily().Times(0).Encode();
        }

        String query = "INSERT INTO userevents(id,username,eventname,timestart,timeend,description,rec) values('" + eventId + "','"
            + Session["user"].ToString() + "','" + eventName.Text + "','" + start.ToString() + "','" + end.ToString() + "','"
            + description.Text +"','" + recurrence + "');";
        cmd = new MySqlCommand(query, conn);
        cmd.ExecuteReader();
        conn.Close();
        Response.Redirect(Request.RawUrl);
       // Response.Write("<script language=\"javascript\">window.open('LoginView.aspx','_self')</script>");

    }
    protected void Button9_Click(object sender, EventArgs e)
    {
        Response.Redirect("AddClass.aspx");
    }

    protected void DayView_EventMenuClick1(object sender, DayPilot.Web.Ui.Events.EventMenuClickEventArgs e)
    {
        String deletequery;
        if (e.Command == "Delete")
        {
            if (e.Recurrent)
            {
                String getSemester = "SELECT semester from userevents WHERE id= " + e.RecurrentMasterId.ToString() + ";";
                MySqlCommand cmd = new MySqlCommand(getSemester,conn);
                MySqlDataReader dr = cmd.ExecuteReader();
                dr.Read();
                String semester = dr["semester"].ToString();
                dr.Close();
                if (semester != "None")
                {
                    String deleteClass = "DELETE FROM class WHERE Semester='" + semester + "' AND ClassName='" + e.Text + "' AND user='" + Session["user"] + "';";
                    cmd = new MySqlCommand(deleteClass, conn);
                    reader = cmd.ExecuteReader();
                    reader.Close();
                }

                deletequery = "DELETE FROM userevents WHERE id = " + e.RecurrentMasterId.ToString() + ";";
                cmd = new MySqlCommand(deletequery, conn);
                reader = cmd.ExecuteReader();
                reader.Close();
                DataTable userEvents = new DataTable();
                
                String getEventsQuery = "SELECT * FROM userevents WHERE username='" + Session["user"] + "';";
                MySqlCommand cmd2 = new MySqlCommand(getEventsQuery, conn);
                MySqlDataAdapter da = new MySqlDataAdapter(cmd2);
                da.Fill(userEvents);

                if (userEvents.Rows.Count >= 1)
                {
                    DayView.DataSource = userEvents;
                    DayView.DataStartField = "timestart";
                    DayView.DataEndField = "timeend";
                    DayView.DataTextField = "eventname";
                    DayView.DataIdField = "id";
                    DayView.DataRecurrenceField = "rec";

                    cmd.ExecuteReader().Close();
                    conn.Close();
                    DataBind();
                    DayView.Update();
                }
            }
        }
        else if (e.Command == "Edit")
        {
            Session["eventId"] = e.RecurrentMasterId;
            Response.Redirect("EditEvent.aspx");

        }
    }

    protected void Button13_Click(object sender, EventArgs e)
    {
        Response.Redirect("AddSemester.aspx");
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("MainView.aspx");
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Response.Redirect("AddEvent.aspx");
    }
    protected void Button12_Click(object sender, EventArgs e)
    {
        Response.Redirect("ViewSemester.aspx");
    }
    protected void Button14_Click(object sender, EventArgs e)
    {
        Response.Redirect("EditSemester.aspx");
    }
    protected void Button15_Click(object sender, EventArgs e)
    {
        Response.Redirect("RemoveSemester.aspx");
    }
    protected void Button16_Click(object sender, EventArgs e)
    {
        Response.Redirect("About.aspx");
    }
    protected void Button5_Click(object sender, EventArgs e)
    {
        Response.Redirect("AddProfessor.aspx");
    }
    protected void Button10_Click(object sender, EventArgs e) // Edit Class
    {
        Response.Redirect("EditClass.aspx");
    }
    protected void Button11_Click(object sender, EventArgs e)
    {
        Response.Redirect("RemoveClass.aspx");
    }
    protected void Button8_Click(object sender, EventArgs e)
    {
        Response.Redirect("ViewClass.aspx");
    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        Response.Redirect("ViewProfessor.aspx");
    }
    protected void Button6_Click(object sender, EventArgs e)
    {
        Response.Redirect("EditProfessor.aspx");
    }
    protected void Buttoneditsem_Click(object sender, EventArgs e)
    {
       // Response.Redirect("EditSemester.aspx");
    }
    protected void Button7_Click(object sender, EventArgs e)
    {
        Response.Redirect("RemoveProfessor.aspx");
    }
    protected void Button17_Click(object sender, EventArgs e)
    {
        Response.Redirect("Help.aspx");
    }
    protected void Button19_Click(object sender, EventArgs e)
    {
        Response.Redirect("Feedback.aspx");
    }
}