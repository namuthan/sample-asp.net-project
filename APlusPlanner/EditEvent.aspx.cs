﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DayPilot.Web.Ui.Recurrence;
using MySql.Data;
using MySql.Data.MySqlClient;

public partial class EditEvent : System.Web.UI.Page
{
    public MySqlConnection conn;
    public MySqlDataReader reader;
    public MySqlDataAdapter adapter;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoggedIn"] == null || Session["LoggedIn"] != "1")
        {
            Response.Redirect("LoginView.aspx");
        }
        conn = new MySqlConnection();
        conn.ConnectionString = "server=70.75.191.26; user id=root; password=; database=aplusplus;Allow Zero Datetime=True";
        conn.Open();

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        String recurrence;
        String eventId = Session["EventId"].ToString();
        String newEventName = eventName.Text;
        String newEventType = eventType.SelectedValue;
        DateTime start = new DateTime(Int32.Parse(startyear.Text), Int32.Parse(startmonth.Text), Int32.Parse(startday.Text), Int32.Parse(starthour.Text), Int32.Parse(startminute.Text), 0);
        DateTime end = new DateTime(Int32.Parse(startyear.Text), Int32.Parse(startmonth.Text), Int32.Parse(startday.Text), Int32.Parse(endhour.Text), Int32.Parse(endminute.Text), 0);
        DateTime until = new DateTime(Int32.Parse(endyear.Text), Int32.Parse(endmonth.Text), Int32.Parse(endday.Text), Int32.Parse(endhour.Text), Int32.Parse(endminute.Text), 0);
        String newDescription = description.Text;

        if (recDrop.SelectedValue == "daily")
        {
            recurrence = RecurrenceRule.FromDateTime(eventId, start).Daily().Until(end).Encode();
        }
        else if (recDrop.SelectedValue == "weekly")
        {
            recurrence = RecurrenceRule.FromDateTime(eventId.ToString(), start).Weekly().Until(until).Encode();
        }
        else if (recDrop.SelectedValue == "monthly")
        {
            recurrence = RecurrenceRule.FromDateTime(eventId.ToString(), start).Monthly().Until(until).Encode();
        }
        else
        {
            recurrence = RecurrenceRule.FromDateTime(eventId.ToString(), start).Daily().Until(until).Encode();
        }

        String query = "UPDATE userevents SET eventName='" + newEventName + "', tag='" + newEventType + "', timestart='" 
            + start.ToString() + "', timeend='" + end.ToString()
            + "', description='" + newDescription + "', rec='" + recurrence + "', untildate='" + until.ToString() + "' WHERE id='"
            + eventId + "';";

        MySqlCommand cmd = new MySqlCommand(query, conn);
        cmd.ExecuteReader();
        conn.Close();
        Response.Redirect("MainView.aspx");

    }

    public void ASPNET_MsgBox(String message)
    {
        System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">" + System.Environment.NewLine);

        System.Web.HttpContext.Current.Response.Write("alert(\"" + message + "\")" + System.Environment.NewLine);

        System.Web.HttpContext.Current.Response.Write("</SCRIPT>");
    }
}