﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewClass.aspx.cs" Inherits="Professor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
     <form id="form1" runat="server">
        <div style="height: 164px; margin-left: 120px; width: 1734px;">
            <asp:Panel ID="Panel5" runat="server" Height="55px">
                <asp:Panel ID="Panel3" runat="server" Height="55px">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="MS Reference Sans Serif" Font-Size="XX-Large" Text="A+ Student Planner - Web"></asp:Label>
                    <br />
                </asp:Panel>
                 <br />
             <asp:Panel ID="Panel2" runat="server" Height="20px" Width="1400px" Style="float: left;">
                     <asp:LinkButton ID="LinkButton1" runat="server" Style="margin-left:0px;" OnClick="HomeButton1_Click" Font-Names="Microsoft Sans Serif" Font-Underline="False" ForeColor="Black">Home</asp:LinkButton>
                    <br />
                </asp:Panel>
                <asp:Panel ID="Panel4" runat="server" Height="16px" Style="float: left;">
                    <hr style="width: 800px; height: -12px;" />
                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Names="MS Reference Sans Serif" Font-Size="Large" Text="View Class"></asp:Label>
                    <br />
                       <br />
                </asp:Panel>
             <br />
                <asp:Panel ID="Panel10" runat="server" Height="32px" Style="float: left;" Width="200px">
                        <br />
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="Panel7" runat="server" Height="103px" Width="1393px">
                <br />
                <br />
                <br />
            </asp:Panel>
 
            <asp:Panel ID="Panel1" runat="server" Height="654px" Width="1291px"  style="margin-left:80px;">
                <%--login--%>
                <asp:Label ID="Label5" runat="server" Text="Class Name:" Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:DropDownList ID="ClassDropDown" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Height="25px" style="margin-left: 0px;" Width="400px">
                </asp:DropDownList>
                <br />
                <br />
                <asp:Button ID="Button2" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" OnClick="Button2_Click" style="margin-left:450px;" Text="Search" Width="100px" />
                <br />
                <br />
                <asp:Label ID="Label6" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="Class Type:" Width="150px"></asp:Label>
                <asp:Label ID="typeLabel" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="-" Width="400px"></asp:Label>
                <br />
                <br />
                <asp:Label ID="Label3" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="Professor:" Width="150px"></asp:Label>
                <asp:Label ID="profLabel" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="-" Width="400px"></asp:Label>
                <br />
                    <br />
                <asp:Label ID="Label4" runat="server" Text="Semester:"  Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:Label ID="semesterLabel" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="-" Width="400px"></asp:Label>
                <br />
                <br />
                <asp:Label ID="Label9" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="Start Time:" Width="150px"></asp:Label>
                <asp:Label ID="startLabel" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="-" Width="400px"></asp:Label>
                 <br />
                <br />
                <asp:Label ID="Label10" runat="server" Text="End Time:"  Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                    <asp:Label ID="endLabel" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="-" Width="400px"></asp:Label>
                <br />
                <br />
                <asp:Label ID="Label7" runat="server" Text="Days:"  Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:CheckBox ID="CheckMon" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Text="Monday" Style="margin-left:0px;" onclick="javascript: return false;"/>
                <br />
                 <asp:CheckBox ID="CheckTues" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Text="Tuesday" Style="margin-left:150px;" onclick="javascript: return false;"/>
                <br />

                <asp:CheckBox ID="CheckWed" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Text="Wednesday" Style="margin-left:150px;" onclick="javascript: return false;"/>
                <br />

<asp:CheckBox ID="CheckThurs" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Text="Thursday" Style="margin-left:150px;" onclick="javascript: return false;"/>
                <br />

<asp:CheckBox ID="CheckFri" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Text="Friday" Style="margin-left:150px;" onclick="javascript: return false;"/>
                <br />

<asp:CheckBox ID="CheckSat" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Text="Saturday" Style="margin-left:150px;" onclick="javascript: return false;"/>
                <br />

<asp:CheckBox ID="CheckSun" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Text="Sunday" Style="margin-left:150px;" onclick="javascript: return false;"/>
                <br />
                 <br />
                 <asp:Label ID="Label20" runat="server" Text="Description:"  Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                    <asp:TextBox ID="description" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="MultiLine" Width="560px" Height="151px" ReadOnly="True"></asp:TextBox>
                    <br />
            </asp:Panel>
                 <asp:Panel ID="Panel9" runat="server" Height="16px" Style="float:left;" Width="594px">
                    <hr style="width: 800px; float:left; margin-left:0px;"/>
                    <br />
                </asp:Panel>
             <asp:Panel ID="Panel11" runat="server" Height="16px" Style="float:left;" Width="986px">
                <br />
                    <br />
                </asp:Panel>
                         <asp:Panel ID="Panel8" runat="server" Height="50px" Width="1393px" style="margin-left:180px;">
                <br />
            </asp:Panel>
<br />

        </div>
    </form>
</body>
</html>
