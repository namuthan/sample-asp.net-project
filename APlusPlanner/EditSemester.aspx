﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditSemester.aspx.cs" Inherits="Professor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
     <form id="form1" runat="server">
        <div style="height: 728px; margin-left: 120px; width: 1734px;">
            <asp:Panel ID="Panel5" runat="server" Height="55px">
                <asp:Panel ID="Panel3" runat="server" Height="45px">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="MS Reference Sans Serif" Font-Size="XX-Large" Text="A+ Student Planner - Web"></asp:Label>
                    <br />
                </asp:Panel>
                           <br />
             <asp:Panel ID="Panel2" runat="server" Height="16px" Width="1400px" Style="float: left;">
                     <asp:LinkButton ID="LinkButton1" runat="server" Style="margin-left:0px;" OnClick="HomeButton1_Click" Font-Names="Microsoft Sans Serif" Font-Underline="False" ForeColor="Black">Home</asp:LinkButton>
                    <br />
                </asp:Panel>
            <br />
                <asp:Panel ID="Panel4" runat="server" Height="16px" Style="float: left;">
                    <hr style="width: 800px; height: -12px;" />
                    <br />
                       <br />
                </asp:Panel>
             <br />
                <asp:Panel ID="Panel10" runat="server" Height="42px" Style="float: left;" Width="200px">
                        <br />

                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Names="MS Reference Sans Serif" Font-Size="Large" Text="Edit Semester"></asp:Label>
                      <br />
                </asp:Panel>
            </asp:Panel>
  <br />
                <br />
            <asp:Panel ID="Panel7" runat="server" Height="89px" Width="1393px">
                <br />
                <br />
                <br />
            </asp:Panel>
            <asp:Panel ID="Panel1" runat="server" Height="703px" Width="1291px"  style="margin-left:80px;">
                <%--login--%>
                <asp:Label ID="Label5" runat="server" Text="Semesters:" Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                  <br />
                <asp:TextBox ID="semList" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Height="177px" TextMode="MultiLine" Width="400px" style="margin-left:150px;"></asp:TextBox>
                <br />
                <br />
                <br />
                <asp:Label ID="Label3" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="Semester Name:" Width="150px" Style="margin-left: 0px;"></asp:Label>
                <asp:TextBox ID="semSearch" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="400px"></asp:TextBox>
                <br />
                <asp:Button ID="Button2" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" OnClick="Button1_Click" style="margin-left:450px;" Text="Search" Width="110px" />
                <br />
                <br />

                  <asp:Label ID="Label9" runat="server" Text="Start Date:" Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                 <asp:Label ID="startDate" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="yyyy-mm-dd" Width="150px"  Style="margin-left:0px;"></asp:Label>
                 <br />
                <br />
                <asp:Label ID="Label10" runat="server" Text="End Date:"  Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:Label ID="endDate" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="yyyy-mm-dd" Width="150px"  Style="margin-left:0px;"></asp:Label>
                <br />
                 <br />
                <br />
                <br />
                  <asp:Label ID="Label4" runat="server" Text="New Name:" Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:TextBox ID="semesterName" runat="server"  Font-Size="Large" Width="400px" Font-Names="Microsoft Sans Serif" ></asp:TextBox>
                <br />
                    <br />
               
            
                  <asp:Label ID="Label6" runat="server" Text="New Start Date:" Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:TextBox ID="startYear" runat="server"  Font-Size="Large" Width="80px" Font-Names="Microsoft Sans Serif" TextMode="DateTime" PlaceHolder="yyyy"></asp:TextBox>
                    <asp:Label ID="Label12" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="-" Width="22px" Style="margin-left:10px;"></asp:Label>
                    <asp:TextBox ID="startMonth" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" PlaceHolder="mm"></asp:TextBox>
                 <asp:Label ID="Label7" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="-" Width="22px" Style="margin-left:10px;"></asp:Label>
                    <asp:TextBox ID="startDay" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" PlaceHolder="dd"></asp:TextBox>
                 <asp:Label ID="Label15" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="yyyy-mm-dd" Width="150px"  Style="margin-left:20px;"></asp:Label>
                 <br />
                <br />
                <asp:Label ID="Label8" runat="server" Text="New End Date:"  Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                    <asp:TextBox ID="endYear" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="80px" PlaceHolder="yyyy"></asp:TextBox>
                    <asp:Label ID="Label13" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Style="margin-left:10px;" Text="-" Width="22px"></asp:Label>
                    <asp:TextBox ID="endMonth" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" PlaceHolder="mm"></asp:TextBox>
                <asp:Label ID="Label14" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Style="margin-left: 10px;" Text="-" Width="22px"></asp:Label>
                <asp:TextBox ID="endDay" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" PlaceHolder="dd"></asp:TextBox>
                <asp:Label ID="Label16" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="yyyy-mm-dd" Width="150px"  Style="margin-left:20px;"></asp:Label>
                <br />
                <br />
                <br />
  <asp:Button ID="Button3" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" OnClick="Button2_Click" style="margin-left:450px;" Text="Submit" Width="110px" />
                <br />
                <br />

                <br />
                <br />

            </asp:Panel>
                 <asp:Panel ID="Panel9" runat="server" Height="16px" Style="float:left;" Width="594px">
                    <hr style="width: 800px; float:left; margin-left:0px;"/>
                    <br />
                </asp:Panel>
             <asp:Panel ID="Panel11" runat="server" Height="16px" Style="float:left;" Width="986px">
                <br />
                    <br />
                </asp:Panel>
                         <asp:Panel ID="Panel8" runat="server" Height="50px" Width="1393px" style="margin-left:180px;">
                <br />
            </asp:Panel>
<br />

        </div>
    </form>
</body>
</html>
