﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RegistrationView.aspx.cs" Inherits="RegistrationView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .inlineBlock {
        }
    </style>
    <link href="MainView.css" rel="stylesheet" type="text/css" />

</head>
<body style="height: 829px">
    <form id="form1" runat="server">
        <div style="height: 728px; margin-left: 120px; width: 1734px;">
            <asp:Panel ID="Panel5" runat="server" Height="55px">
                <asp:Panel ID="Panel3" runat="server" Height="55px">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="MS Reference Sans Serif" Font-Size="XX-Large" Text="A+ Student Planner - Web"></asp:Label>
                    <br />
                </asp:Panel>
                <asp:Panel ID="Panel4" runat="server" Height="23px" Style="float: left;">
                    <hr style="width: 1600px" />
                    <br />
                </asp:Panel>
                <asp:Panel ID="Panel10" runat="server" Height="42px" Style="float: left;" Width="200px">
                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Names="MS Reference Sans Serif" Font-Size="Large" Text="Registration"></asp:Label>
                </asp:Panel>
                           <asp:Panel ID="Panel2" runat="server" Height="130px" Width="1393px">
                <br />
                <br />
                <br />
            </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="Panel7" runat="server" Height="89px" Width="1393px">
                <br />
                <br />
                <br />
            </asp:Panel>
            <asp:Panel ID="Panel1" runat="server" Height="210px" Width="1291px"  style="margin-left:80px;">
                <%--login--%>
                <asp:Label ID="Label5" runat="server" Text="Username:" Font-Size="Large" Width="200px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:TextBox ID="username" runat="server"  Font-Size="Large" Width="400px" Font-Names="Microsoft Sans Serif" ></asp:TextBox>
                <asp:Label ID="loginFail1" runat="server" Font-Names="Microsoft Sans Serif" style="margin-left:10px;" Text="Please check username (possible duplicate)"></asp:Label>
                <br />
                <br />
                <asp:Label ID="Label4" runat="server" Text="Email:" Font-Size="Large" Width="200px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:TextBox ID="emailinput" runat="server"  Font-Size="Large" Width="400px" Font-Names="Microsoft Sans Serif" ></asp:TextBox>
                <asp:Label ID="emaillabel" runat="server" Font-Names="Microsoft Sans Serif" style="margin-left:10px;" Text="Please check email (possible duplicate)"></asp:Label>
                <br />
                <br />
                <asp:Label ID="Label6" runat="server" Text="Password:"  Font-Size="Large" Width="200px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:TextBox ID="password" runat="server"  Font-Size="Large" Width="400px" TextMode="Password" Font-Names="Microsoft Sans Serif" ></asp:TextBox>
                <asp:Label ID="loginFail0" runat="server" Font-Names="Microsoft Sans Serif" style="margin-left:10px;" Text="Please check password"></asp:Label>
                <br />
                <br />
                <asp:Label ID="Label3" runat="server" Text="Password Check:"  Font-Size="Large" Width="200px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:TextBox ID="confirmpassword" runat="server"  Font-Size="Large" Width="400px" TextMode="Password" Font-Names="Microsoft Sans Serif" ></asp:TextBox>
                <asp:Label ID="confirmpasswordlabel" runat="server" Font-Names="Microsoft Sans Serif" style="margin-left:10px;" Text="Please check password"></asp:Label>
                <br />
                <br />
                

            </asp:Panel>
             <asp:Panel ID="Panel6" runat="server" Height="70px" Width="1393px" style="margin-left:130px;">
                <asp:Button ID="Button1" runat="server" Text="Register" style="margin-left:310px;" Font-Names="Microsoft Sans Serif" Font-Size="Large" Width="100px" OnClick="Button1_Click"  />
                   <br />
                   <asp:Label ID="loginFail" runat="server" Text="Please try again" Font-Names="Microsoft Sans Serif" style="margin-left:300px;" Visible="False"></asp:Label>
                   <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Button" />
                   <br />
            </asp:Panel>
                         <asp:Panel ID="Panel8" runat="server" Height="50px" Width="1393px" style="margin-left:180px;">
                <br />
            </asp:Panel>
<br />

        </div>
    </form>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js
"></script>
    ;
    <script type="text/javascript">

        $(document).ready(function () {
            $(".dropdown").hover(function () {
                $(".submenu").slideToggle(400);
            });

            //$(".account").click(function () {
            //    var X = $(this).attr('id');
            //    if (X == 1) {
            //        $(".submenu").hide();
            //        $(this).attr('id', '0');
            //    }
            //    else {
            //        $(".submenu").show();
            //        $(this).attr('id', '1');
            //    }

            //});

            ////Mouse click on sub menu
            //$(".submenu").mouseup(function () {
            //    return false
            //});

            ////Mouse click on my account link
            //$(".account").mouseup(function () {
            //    return false
            //});


            ////Document Click
            //$(document).mouseup(function () {
            //    $(".submenu").hide();
            //    $(".account").attr('id', '');
            // });
        });
    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".dropdown2").hover(function () {
                $(".submenu2").slideToggle(400);
            });
        });
    </script>

</body>

</html>


