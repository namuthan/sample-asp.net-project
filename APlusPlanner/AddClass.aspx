﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddClass.aspx.cs" Inherits="Professor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
     <form id="form1" runat="server">
        <div style="height: 728px; margin-left: 120px; width: 1734px;">
            <asp:Panel ID="Panel5" runat="server" Height="55px">
                <asp:Panel ID="Panel3" runat="server" Height="55px">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="MS Reference Sans Serif" Font-Size="XX-Large" Text="A+ Student Planner - Web"></asp:Label>
                    <br />
                </asp:Panel>
                             <br />
             <asp:Panel ID="Panel2" runat="server" Height="16px" Width="1400px" Style="float: left;">
                     <asp:LinkButton ID="LinkButton1" runat="server" Style="margin-left:0px;" OnClick="HomeButton2_Click" Font-Names="Microsoft Sans Serif" Font-Underline="False" ForeColor="Black">Home</asp:LinkButton>
                    <br />
                </asp:Panel>
            <br />
                <asp:Panel ID="Panel4" runat="server" Height="16px" Style="float: left;">
                    <hr style="width: 800px; height: -12px;" />
                    <br />
                       <br />
                </asp:Panel>
             <br />
                <asp:Panel ID="Panel10" runat="server" Height="42px" Style="float: left;" Width="200px">
                        <br />
                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Names="MS Reference Sans Serif" Font-Size="Large" Text="New Class"></asp:Label>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="Panel7" runat="server" Height="106px" Width="1393px">
                <br />
                <br />
                <br />
            </asp:Panel>
            <asp:Panel ID="Panel1" runat="server" Height="647px" Width="1291px"  style="margin-left:80px;">
                <%--login--%>
                <asp:Label ID="Label5" runat="server" Text="Class Name:" Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:TextBox ID="eventName" runat="server"  Font-Size="Large" Width="400px" Font-Names="Microsoft Sans Serif" ></asp:TextBox>
                <br />
                    <br />
                 <asp:Label ID="Label6" runat="server" Text="Class Type:"  Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:DropDownList ID="typeDrop" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Style="margin-left:0px;" Width="200px" OnSelectedIndexChanged="typeDrop_SelectedIndexChanged">
                    <asp:ListItem Value="0">type</asp:ListItem>
                    <asp:ListItem Value="lecture">lecture</asp:ListItem>
                    <asp:ListItem>tutorial</asp:ListItem>
                    <asp:ListItem>lab</asp:ListItem>
                </asp:DropDownList>
                <br />
                    <br />
                <asp:Label ID="Label3" runat="server" Text="Professor"  Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:TextBox ID="profBox" runat="server"  Font-Size="Large" Width="400px" Font-Names="Microsoft Sans Serif" ></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label4" runat="server" Text="Semester:"  Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:DropDownList ID="SemesterDropDown" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Width="200px" style="margin-left:0px;"></asp:DropDownList>
                <br />
                    <br />
                  <asp:Label ID="Label9" runat="server" Text="Start Time:" Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:TextBox ID="eventStartHour" runat="server"  Font-Size="Large" Width="32px" Font-Names="Microsoft Sans Serif" TextMode="DateTime" PlaceHolder="HH"></asp:TextBox>
                    <asp:Label ID="Label12" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text=" : " Width="22px" Style="margin-left:10px;"></asp:Label>
                    <asp:TextBox ID="eventStartMinute" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" PlaceHolder="MM"></asp:TextBox>
                 <br />
                <br />
                <asp:Label ID="Label10" runat="server" Text="End Time:"  Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                    <asp:TextBox ID="eventEndHour" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" PlaceHolder="HH"></asp:TextBox>
                    <asp:Label ID="Label13" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Style="margin-left:10px;" Text=" : " Width="22px"></asp:Label>
                    <asp:TextBox ID="eventEndMinute" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" PlaceHolder="MM"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label7" runat="server" Text="Days:"  Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:CheckBox ID="CheckMon" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Text="Monday" Style="margin-left:0px;"/>
                <br />
                 <asp:CheckBox ID="CheckTues" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Text="Tuesday" Style="margin-left:150px;"/>
                <br />

                <asp:CheckBox ID="CheckWed" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Text="Wednesday" Style="margin-left:150px;"/>
                <br />

<asp:CheckBox ID="CheckThurs" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Text="Thursday" Style="margin-left:150px;"/>
                <br />

<asp:CheckBox ID="CheckFri" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Text="Friday" Style="margin-left:150px;"/>
                <br />

<asp:CheckBox ID="CheckSat" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Text="Saturday" Style="margin-left:150px;"/>
                <br />

<asp:CheckBox ID="CheckSun" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Text="Sunday" Style="margin-left:150px;"/>
                <br />
                 <br />
                 <asp:Label ID="Label20" runat="server" Text="Description:"  Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                    <asp:TextBox ID="description" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="MultiLine" Width="560px" Height="151px"></asp:TextBox>
                    <br />
                <br />
            </asp:Panel>
             <asp:Panel ID="Panel6" runat="server" Height="70px" Width="1393px" style="margin-left:545px;">
                <asp:Button ID="Button1" runat="server" Text="Submit" style="margin-left:150px;" Font-Names="Microsoft Sans Serif" Font-Size="Large" Width="100px" OnClick="Button1_Click"  />
                   <br />
                  <asp:Label ID="loginFail" runat="server" Font-Names="Microsoft Sans Serif" style="margin-left:140px;" Text="Please try again" Visible="False"></asp:Label>

                <br />
            </asp:Panel>
                 <asp:Panel ID="Panel9" runat="server" Height="16px" Style="float:left;" Width="594px">
                    <hr style="width: 800px; float:left; margin-left:0px;"/>
                    <br />
                </asp:Panel>
             <asp:Panel ID="Panel11" runat="server" Height="16px" Style="float:left;" Width="986px">
                <br />
                    <br />
                </asp:Panel>
                         <asp:Panel ID="Panel8" runat="server" Height="50px" Width="1393px" style="margin-left:180px;">
                <br />
            </asp:Panel>
<br />

        </div>
    </form>
</body>
</html>
