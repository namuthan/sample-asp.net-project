﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using MySql.Data;
using DayPilot;
using DayPilot.Web.Ui.Recurrence;

public partial class Professor : System.Web.UI.Page
{
    public MySqlConnection conn;
    public MySqlDataReader reader;
    public MySqlDataAdapter adapter;

    public String redirect;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        conn = new MySqlConnection();
        conn.ConnectionString = "server=70.75.191.26; user id=root; password=; database=aplusplus;Allow Zero Datetime=True";
        conn.Open();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        MySqlCommand cmd;

        String selectLast = "SELECT id FROM userevents ORDER BY ID DESC LIMIT 1;";
        cmd = new MySqlCommand(selectLast, conn);
        reader = cmd.ExecuteReader();
        reader.Read();
        int id = reader.GetInt32(0);
        id += 1;
        String eventId = id.ToString();
        reader.Close();

        conn = new MySqlConnection();
        conn.ConnectionString = "server=70.75.191.26; user id=root; password=; database=aplusplus";
        conn.Open();

        DateTime start = new DateTime(Int32.Parse(startYear.Text), Int32.Parse(startMonth.Text), Int32.Parse(startDay.Text), Int32.Parse(eventStartHour.Text), Int32.Parse(eventStartMinute.Text), 0);

        DateTime end = new DateTime(Int32.Parse(startYear.Text), Int32.Parse(startMonth.Text), Int32.Parse(startDay.Text), Int32.Parse(eventEndHour.Text), Int32.Parse(eventEndMinute.Text), 0);

        DateTime until = new DateTime(Int32.Parse(endYear.Text), Int32.Parse(endMonth.Text), Int32.Parse(endDay.Text), Int32.Parse(eventEndHour.Text), Int32.Parse(eventEndMinute.Text), 0);

        String recurrence;

        if (recDrop.SelectedValue == "daily")
        {
            recurrence = RecurrenceRule.FromDateTime(id.ToString(), start).Daily().Until(until).Encode();
        }
        else if (recDrop.SelectedValue == "weekly")
        {
            recurrence = RecurrenceRule.FromDateTime(id.ToString(), start).Weekly().Until(until).Encode();
        }
        else if (recDrop.SelectedValue == "monthly")
        {
            recurrence = RecurrenceRule.FromDateTime(id.ToString(), start).Monthly().Until(until).Encode();
        }
        else
        {
            recurrence = RecurrenceRule.FromDateTime(id.ToString(), start).Daily().Times(0).Encode();
        }

        String tag = "event";

        String query = "INSERT INTO userevents(id,username,eventname,tag,timestart,timeend,description,rec,untildate) values('" + eventId + "','"
     + Session["user"].ToString() + "','" + eventName.Text + "','" + tag + "','" + start.ToString() + "','" + end.ToString() + "','"
     + description.Text + "','" + recurrence + "','" + until.ToString() + "');";
        cmd = new MySqlCommand(query, conn);
        cmd.ExecuteReader();
        conn.Close();
        Response.Redirect("MainView.aspx");
    }
    protected void HomeButton1_Click(object sender, EventArgs e)
    {
        Response.Redirect("MainView.aspx");
    }
}