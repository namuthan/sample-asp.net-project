﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddSemester.aspx.cs" Inherits="Professor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
     <form id="form1" runat="server">
        <div style="height: 728px; margin-left: 120px; width: 1734px;">
            <asp:Panel ID="Panel5" runat="server" Height="55px">
                <asp:Panel ID="Panel3" runat="server" Height="42px">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="MS Reference Sans Serif" Font-Size="XX-Large" Text="A+ Student Planner - Web"></asp:Label>
                    <br />
                </asp:Panel>
                                       <br />
             <asp:Panel ID="Panel2" runat="server" Height="25px" Width="1400px" Style="float: left;">
                     <asp:LinkButton ID="LinkButton1" runat="server" Style="margin-left:0px;" OnClick="HomeButton1_Click" Font-Names="Microsoft Sans Serif" Font-Underline="False" ForeColor="Black">Home</asp:LinkButton>
                    <br />
                </asp:Panel>
                <br />
                <asp:Panel ID="Panel4" runat="server" Height="16px" Style="float: left;" Width="800px">
                    <hr style="width: 799px; height: -8px; margin-bottom: 0px;" />
                    <br />
                       <br />
                </asp:Panel>
             <br />
                <br />
                <asp:Panel ID="Panel10" runat="server" Height="42px" Style="float: left;" Width="200px">
    
                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Names="MS Reference Sans Serif" Font-Size="Large" Text="New Semester"></asp:Label>
                </asp:Panel>
            </asp:Panel>
            <br />
                <br />
            <asp:Panel ID="Panel7" runat="server" Height="117px" Width="1393px">
            </asp:Panel>
            <asp:Panel ID="Panel1" runat="server" Height="176px" Width="1291px"  style="margin-left:80px;">
            
                <asp:Label ID="Label5" runat="server" Text="Semester Name:" Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:TextBox ID="semesterName" runat="server"  Font-Size="Large" Width="400px" Font-Names="Microsoft Sans Serif" ></asp:TextBox>
                <br />
                    <br />
               
            
                  <asp:Label ID="Label9" runat="server" Text="Start Date:" Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:TextBox ID="startYear" runat="server"  Font-Size="Large" Width="80px" Font-Names="Microsoft Sans Serif" TextMode="DateTime" PlaceHolder="yyyy"></asp:TextBox>
                    <asp:Label ID="Label12" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="-" Width="22px" Style="margin-left:10px;"></asp:Label>
                    <asp:TextBox ID="startMonth" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" PlaceHolder="mm"></asp:TextBox>
                 <asp:Label ID="Label3" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="-" Width="22px" Style="margin-left:10px;"></asp:Label>
                    <asp:TextBox ID="startDay" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" PlaceHolder="dd"></asp:TextBox>
                 <asp:Label ID="Label15" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="yyyy-mm-dd" Width="150px"  Style="margin-left:20px;"></asp:Label>
                 <br />
                <br />
                <asp:Label ID="Label10" runat="server" Text="End Date:"  Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                    <asp:TextBox ID="endYear" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="80px" PlaceHolder="yyyy"></asp:TextBox>
                    <asp:Label ID="Label13" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Style="margin-left:10px;" Text="-" Width="22px"></asp:Label>
                    <asp:TextBox ID="endMonth" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" PlaceHolder="mm"></asp:TextBox>
                <asp:Label ID="Label14" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Style="margin-left: 10px;" Text="-" Width="22px"></asp:Label>
                <asp:TextBox ID="endDay" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" PlaceHolder="dd"></asp:TextBox>
                <asp:Label ID="Label16" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="yyyy-mm-dd" Width="150px"  Style="margin-left:20px;"></asp:Label>
                <br />
                <br />
                <br />
                <br />

            </asp:Panel>
             <asp:Panel ID="Panel6" runat="server" Height="70px" Width="1393px" style="margin-left:380px;">
                <asp:Button ID="Button1" runat="server" Text="Submit" style="margin-left:150px;" Font-Names="Microsoft Sans Serif" Font-Size="Large" Width="100px" OnClick="Button1_Click"  />
                   <br />
                  <asp:Label ID="loginFail" runat="server" Font-Names="Microsoft Sans Serif" style="margin-left:140px;" Text="Please try again" Visible="False"></asp:Label>

                <br />
            </asp:Panel>
                 <asp:Panel ID="Panel9" runat="server" Height="16px" Style="float:left;" Width="594px">
                    <hr style="width: 800px; float:left; margin-left:0px;"/>
                    <br />
                </asp:Panel>
             <asp:Panel ID="Panel11" runat="server" Height="16px" Style="float:left;" Width="986px">
                <br />
                    <br />
                </asp:Panel>
                         <asp:Panel ID="Panel8" runat="server" Height="50px" Width="1393px" style="margin-left:180px;">
                <br />
            </asp:Panel>
<br />

        </div>
    </form>
</body>
</html>
