﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using MySql.Data;
using DayPilot;
using DayPilot.Web.Ui.Recurrence;

public partial class Professor : System.Web.UI.Page
{
    public MySqlConnection conn;
    public MySqlDataReader reader;
    public MySqlDataAdapter adapter;
    public int id;

    public String redirect;
    String recurrence;

    protected void Page_Load(object sender, EventArgs e)
    {

        ClassDropDown.Items.Clear();     
        if (Session["LoggedIn"] == null || Session["LoggedIn"] != "1")
            {
                Response.Redirect("LoginView.aspx");
            }
            conn = new MySqlConnection();
            conn.ConnectionString = "server=70.75.191.26; user id=root; password=; database=aplusplus;Allow Zero Datetime=True";
            conn.Open();


            String query;
            query = "select * from class where user='" + Session["user"].ToString() + "';";

            MySqlCommand cmd = new MySqlCommand(query, conn);
            reader = cmd.ExecuteReader();
            String input = "";
            String classname = "";

            while (reader.Read())
            {
                input = reader.GetString(reader.GetOrdinal("Semester")) + ": " + reader.GetString(reader.GetOrdinal("ClassName")) + " - " + reader.GetString(reader.GetOrdinal("Prof"));
                classname = reader.GetString(reader.GetOrdinal("ClassName"));
                ClassDropDown.Items.Add(new ListItem(input, classname));
            }
            reader.Close();
    }


    public void ASPNET_MsgBox(String message)
    {
        System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">" + System.Environment.NewLine);

        System.Web.HttpContext.Current.Response.Write("alert(\"" + message + "\")" + System.Environment.NewLine);

        System.Web.HttpContext.Current.Response.Write("</SCRIPT>");
    }
    protected void HomeButton1_Click(object sender, EventArgs e)
    {
        Response.Redirect("MainView.aspx");
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        conn = new MySqlConnection();
        conn.ConnectionString = "server=70.75.191.26; user id=root; password=; database=aplusplus;Allow Zero Datetime=True";
        conn.Open();



        String query;
        query = "select * from class where user='" + Session["user"].ToString() + "' and ClassName='" + ClassDropDown.SelectedValue.ToString() + "';";

        MySqlCommand cmd = new MySqlCommand(query, conn);
        reader = cmd.ExecuteReader();
        String input = "";
        String classname = "";
        String checkmon = "";
        String checktues = "";
        String checkwed = "";
        String checkthurs = "";
        String checkfri = "";
        String checksat = "";
        String checksun = "";


        while (reader.Read())
        {
            profLabel.Text = reader.GetString(reader.GetOrdinal("Prof"));
            semesterLabel.Text = reader.GetString(reader.GetOrdinal("Semester"));
            checkmon = reader.GetString(reader.GetOrdinal("OnMonday"));
            checktues = reader.GetString(reader.GetOrdinal("OnTuesday"));
            checkwed = reader.GetString(reader.GetOrdinal("OnWednesday"));
            checkthurs = reader.GetString(reader.GetOrdinal("OnThursday"));
            checkfri = reader.GetString(reader.GetOrdinal("OnFriday"));
            checksat = reader.GetString(reader.GetOrdinal("OnSaturday"));
            checkmon = reader.GetString(reader.GetOrdinal("OnSunday"));
        }
        reader.Close();

        if (checkmon == "true")
        {
            CheckMon.Checked = true;
        }
        if (checktues == "true")
        {
            CheckTues.Checked = true;
        }
        if (checkwed == "true")
        {
            CheckWed.Checked = true;
        }
        if (checkthurs == "true")
        {
            CheckThurs.Checked = true;
        }
        if (checkfri == "true")
        {
            CheckFri.Checked = true;
        }
        if (checksat == "true")
        {
            CheckSat.Checked = true;
        }
        if (checksun == "true")
        {
            CheckSun.Checked = true;
        }

        query = "select * from userevents where username='" + Session["user"].ToString() + "' and eventname='" + ClassDropDown.SelectedValue.ToString() + "';";

        cmd = new MySqlCommand(query, conn);
        reader = cmd.ExecuteReader();

        while (reader.Read())
        {
            startLabel.Text = reader.GetString(reader.GetOrdinal("timestart"));
            endLabel.Text = reader.GetString(reader.GetOrdinal("timeend"));
            description.Text = reader.GetString(reader.GetOrdinal("description"));
            typeLabel.Text = reader.GetString(reader.GetOrdinal("tag"));
        }
        reader.Close();
        conn.Close();
    }
}