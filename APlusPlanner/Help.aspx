﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Help.aspx.cs" Inherits="Professor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #form1 {
            width: 1139px;
            height: 139px;
        }
    </style>
</head>
<body style="width: 1164px; height: 146px">
     <form id="form1" runat="server">
        <div style="height: 138px; margin-left: 120px; width: 971px;">
            <asp:Panel ID="Panel5" runat="server" Height="55px" Width="1007px">
                <asp:Panel ID="Panel3" runat="server" Height="55px">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="MS Reference Sans Serif" Font-Size="XX-Large" Text="A+ Student Planner - Web"></asp:Label>
                    <br />
                </asp:Panel>
                 <br />
             <asp:Panel ID="Panel2" runat="server" Height="20px" Width="963px" Style="float: left;">
                     <asp:LinkButton ID="LinkButton1" runat="server" Style="margin-left:0px;" OnClick="HomeButton1_Click" Font-Names="Microsoft Sans Serif" Font-Underline="False" ForeColor="Black">Home</asp:LinkButton>
                    <br />
                </asp:Panel>
                <asp:Panel ID="Panel4" runat="server" Height="16px" Style="float: left;">
                    <hr style="width: 800px; height: -12px;" />
                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Names="MS Reference Sans Serif" Font-Size="Large" Text="Help"></asp:Label>
                    <br />
                       <br />
                </asp:Panel>
             <br />
                <asp:Panel ID="Panel10" runat="server" Height="32px" Style="float: left;" Width="200px">
                        <br />
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="Panel7" runat="server" Height="103px" Width="999px">
                <br />
                <br />
                <br />
            </asp:Panel>
            <asp:Panel ID="Panel1" runat="server" Height="406px" Width="651px"  style="margin-left:80px;" Font-Names="Microsoft Sans Serif" Font-Size="Medium">
                <%--login--%>

                Adding an Event:<br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1. Press &quot;Add Event&quot;<br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2. Enter information<br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3. Press &quot;Submit&quot;<br />
                <br />
                Adding a Semester:<br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1. Press &quot;Add Semester&quot;<br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2. Enter information<br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3. Press &quot;Submit&quot;<br />
                <br />
                Adding a Class:<br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1. Add a Semester<br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2. Enter information (Semester name must be an existing semester)<br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3. Press &quot;Submit&quot;<br />
                <br />
                Adding a Professor:<br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1. Press &quot;Add Professor&quot;<br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2. Enter information<br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3. Press &quot;Submit&quot;<br />
                <br />

            </asp:Panel>
                <br />
                 <asp:Panel ID="Panel9" runat="server" Height="16px" Style="float:left;" Width="594px">
                    <hr style="width: 800px; float:left; margin-left:0px;"/>
                    <br />
                </asp:Panel>
<br />

        </div>
    </form>
</body>
</html>
