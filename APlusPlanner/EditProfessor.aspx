﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EditProfessor.aspx.cs" Inherits="Professor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
     <form id="form1" runat="server">
        <div style="height: 728px; margin-left: 120px; width: 1734px;">
            <asp:Panel ID="Panel5" runat="server" Height="55px">
                <asp:Panel ID="Panel3" runat="server" Height="45px">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="MS Reference Sans Serif" Font-Size="XX-Large" Text="A+ Student Planner - Web"></asp:Label>
                    <br />
                </asp:Panel>
                           <br />
             <asp:Panel ID="Panel2" runat="server" Height="16px" Width="1400px" Style="float: left;">
                     <asp:LinkButton ID="LinkButton1" runat="server" Style="margin-left:0px;" OnClick="HomeButton1_Click" Font-Names="Microsoft Sans Serif" Font-Underline="False" ForeColor="Black">Home</asp:LinkButton>
                    <br />
                </asp:Panel>
            <br />
                <asp:Panel ID="Panel4" runat="server" Height="16px" Style="float: left;">
                    <hr style="width: 800px; height: -12px;" />
                    <br />
                       <br />
                </asp:Panel>
             <br />
                <asp:Panel ID="Panel10" runat="server" Height="42px" Style="float: left;" Width="200px">
                        <br />

                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Names="MS Reference Sans Serif" Font-Size="Large" Text="View Professor"></asp:Label>
                      <br />
                </asp:Panel>
            </asp:Panel>
  <br />
                <br />
            <asp:Panel ID="Panel7" runat="server" Height="66px" Width="1393px">
                <br />
                <br />
                <br />
            </asp:Panel>
            <asp:Panel ID="Panel1" runat="server" Height="783px" Width="1291px"  style="margin-left:80px;">
                <br />
                <asp:Label ID="Label3" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="Professor Name:" Width="150px" Style="margin-left: 0px;"></asp:Label>
                <asp:DropDownList ID="ProfessorDropDown" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Height="25px" style="margin-left:0px;" Width="400px">
                </asp:DropDownList>
                <br />
                <br />
                <asp:Button ID="Button2" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" OnClick="Button1_Click" style="margin-left:450px;" Text="Search" Width="100px" />
                <br />
                <br />
                <br />
                <asp:Label ID="Title" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="Title:" Width="150px"></asp:Label>
                <asp:TextBox ID="proftitle" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Width="400px"></asp:TextBox>
                <br />
                    <br />
                <asp:Label ID="FirstName" runat="server" Text="First Name:"  Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif" style="margin-bottom: 0px"></asp:Label>
                <asp:TextBox ID="proffirst" runat="server"  Font-Size="Large" Width="400px" Font-Names="Microsoft Sans Serif" ></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="LastName" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" style="margin-bottom: 0px" Text="Last Name:" Width="150px"></asp:Label>
                <asp:TextBox ID="proflast" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Width="400px"></asp:TextBox>
                <br />
                    <br />
                <asp:Label ID="Email" runat="server" Text="Email:"  Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:TextBox ID="profemail" runat="server"  Font-Size="Large" Width="400px" Font-Names="Microsoft Sans Serif" ></asp:TextBox>
                <br />
                    <br />
                  <asp:Label ID="OfficeLocation" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="Office Location:" Width="150px"></asp:Label>
                <asp:TextBox ID="proflocation" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Width="400px"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="PhoneNumber" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="Phone Number:" Width="150px"></asp:Label>
                <asp:TextBox ID="profphone" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Width="400px"></asp:TextBox>
                <br />
                <br />
                 <asp:Label ID="Label15" runat="server" Text="Office Days:"  Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                        <br />
                <asp:TextBox ID="profdays" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Height="90px" style="margin-left:150px;" TextMode="MultiLine" Width="400px"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label17" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="Office Hours:" Width="150px"></asp:Label>        <br />
                <asp:TextBox ID="profhours" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Height="90px" style="margin-left: 150px;" TextMode="MultiLine" Width="400px"></asp:TextBox>
                <br />
                <br />
                 <asp:Button ID="Submit" runat="server" Text="Submit" style="margin-left:450px;" Font-Names="Microsoft Sans Serif" Font-Size="Large" Width="100px" OnClick="Button2_Click"  />
                   <br />
                  <asp:Label ID="loginFail" runat="server" Font-Names="Microsoft Sans Serif" style="margin-left:450px;" Text="Please try again" Visible="False"></asp:Label>

                <br />
                <br />
                <br />

            </asp:Panel>
                 <asp:Panel ID="Panel9" runat="server" Height="16px" Style="float:left;" Width="594px">
                    <hr style="width: 800px; float:left; margin-left:0px;"/>
                    <br />
                </asp:Panel>
             <asp:Panel ID="Panel11" runat="server" Height="16px" Style="float:left;" Width="986px">
                <br />
                    <br />
                </asp:Panel>
                         <asp:Panel ID="Panel8" runat="server" Height="50px" Width="1393px" style="margin-left:180px;">
                <br />
            </asp:Panel>
<br />

        </div>
    </form>
</body>
</html>
