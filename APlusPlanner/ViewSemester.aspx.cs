﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using MySql.Data;

public partial class Professor : System.Web.UI.Page
{
    public MySqlConnection conn;
    public MySqlDataReader reader;
    public MySqlDataAdapter adapter;

    public String redirect;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        SemesterDropDown.Items.Clear();
        if (Session["LoggedIn"] == null || Session["LoggedIn"] != "1")
        {
            Response.Redirect("LoginView.aspx");
        }
        conn = new MySqlConnection();
        conn.ConnectionString = "server=70.75.191.26; user id=root; password=; database=aplusplus;Allow Zero Datetime=True";
        conn.Open();

        String query = "select * from semester where username='" + Session["user"].ToString() + "';";

        MySqlCommand cmd = new MySqlCommand(query, conn);
        reader = cmd.ExecuteReader();

        while (reader.Read())
        {
            SemesterDropDown.Items.Add(reader.GetString(reader.GetOrdinal("name")));
        }

        reader.Close();

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        MySqlCommand cmd;

        conn = new MySqlConnection();
        conn.ConnectionString = "server=70.75.191.26; user id=root; password=; database=aplusplus;Allow Zero Datetime=True";
        conn.Open();



        String query = "select * from semester where username='" + Session["user"].ToString() + "' and name='" + SemesterDropDown.SelectedValue.ToString() + "';";
        cmd = new MySqlCommand(query, conn);
        reader = cmd.ExecuteReader();
        String start = "";
        String end = "";

        while (reader.Read())
        {
            start = start + reader.GetString(reader.GetOrdinal("startyear")) + "-" + reader.GetString(reader.GetOrdinal("startmonth")) + "-" + reader.GetString(reader.GetOrdinal("startday"));
            end = end + reader.GetString(reader.GetOrdinal("endyear")) + "-" + reader.GetString(reader.GetOrdinal("endmonth")) + "-" + reader.GetString(reader.GetOrdinal("endday"));
        }
        startDate.Text = start;
        endDate.Text = end;
        reader.Close();

        query = "select * from class where user='" + Session["user"].ToString() + "' and Semester='" + SemesterDropDown.SelectedValue.ToString() + "';";
        cmd = new MySqlCommand(query, conn);
        reader = cmd.ExecuteReader();
        String input = "";

        int i = 1;
        while (reader.Read())
        {
            input = input + i +". " + reader.GetString(reader.GetOrdinal("ClassName")) + " - " + reader.GetString(reader.GetOrdinal("Prof")) + "\n";
           
        }
        classList.Text = input;

        conn.Close();
    }
    protected void HomeButton1_Click(object sender, EventArgs e)
    {
        Response.Redirect("MainView.aspx");
    }
}