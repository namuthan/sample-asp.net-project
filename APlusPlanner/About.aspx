﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="About.aspx.cs" Inherits="Professor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #form1 {
            width: 1139px;
            height: 139px;
        }
    </style>
</head>
<body style="width: 1164px; height: 146px">
     <form id="form1" runat="server">
        <div style="height: 138px; margin-left: 120px; width: 971px;">
            <asp:Panel ID="Panel5" runat="server" Height="55px" Width="1007px">
                <asp:Panel ID="Panel3" runat="server" Height="55px">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="MS Reference Sans Serif" Font-Size="XX-Large" Text="A+ Student Planner - Web"></asp:Label>
                    <br />
                </asp:Panel>
                 <br />
             <asp:Panel ID="Panel2" runat="server" Height="20px" Width="963px" Style="float: left;">
                     <asp:LinkButton ID="LinkButton1" runat="server" Style="margin-left:0px;" OnClick="HomeButton1_Click" Font-Names="Microsoft Sans Serif" Font-Underline="False" ForeColor="Black">Home</asp:LinkButton>
                    <br />
                </asp:Panel>
                <asp:Panel ID="Panel4" runat="server" Height="16px" Style="float: left;">
                    <hr style="width: 800px; height: -12px;" />
                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Names="MS Reference Sans Serif" Font-Size="Large" Text="About"></asp:Label>
                    <br />
                       <br />
                </asp:Panel>
             <br />
                <asp:Panel ID="Panel10" runat="server" Height="32px" Style="float: left;" Width="200px">
                        <br />
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="Panel7" runat="server" Height="103px" Width="999px">
                <br />
                <br />
                <br />
            </asp:Panel>
            <asp:Panel ID="Panel1" runat="server" Height="230px" Width="651px"  style="margin-left:80px;" Font-Names="Microsoft Sans Serif" Font-Size="Medium">
                <%--login--%>

                The A+ Student Planner - Web is a&nbsp; APS.NET C# Webforms based application of the open source A+ Student Planner. The user can add events, semesters, classes, and professors to the user&#39;s account. User information is stored in a MySQL Database with all of the users events, classes, semesters, and professors. Classes must be added to an existing semester. Events and classes are displayed on the home page schedule. Users can also edit or delete events, semesters. classes, and professors. The main page display uses DayPilot calendar.<br />
                <br />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; A+ Student Planner: <a href="http://sourceforge.net/projects/studentplanner/">http://sourceforge.net/projects/studentplanner/</a><br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; DayPilot Calendar: <a href="http://www.daypilot.org/">http://www.daypilot.org/</a><br />
                <br />
                Website Made By: Dominic Hul, Tyler Wheeler, Anish Avasthi<br />
                <br />
                <br />

            </asp:Panel>
                <br />
                 <asp:Panel ID="Panel9" runat="server" Height="16px" Style="float:left;" Width="594px">
                    <hr style="width: 800px; float:left; margin-left:0px;"/>
                    <br />
                </asp:Panel>
<br />

        </div>
    </form>
</body>
</html>
