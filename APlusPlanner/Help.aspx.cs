﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Professor : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        
            if (Session["LoggedIn"] == null || Session["LoggedIn"] != "1")
            {
                Response.Redirect("LoginView.aspx");
            }

    }
   
    protected void HomeButton1_Click(object sender, EventArgs e)
    {
        Response.Redirect("MainView.aspx");
    }
}