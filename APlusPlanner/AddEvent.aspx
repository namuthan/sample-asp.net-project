﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddEvent.aspx.cs" Inherits="Professor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
     <form id="form1" runat="server">
        <div style="height: 728px; margin-left: 120px; width: 1734px;">
            <asp:Panel ID="Panel5" runat="server" Height="55px">
                <asp:Panel ID="Panel3" runat="server" Height="55px">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="MS Reference Sans Serif" Font-Size="XX-Large" Text="A+ Student Planner - Web"></asp:Label>
                    <br />
                </asp:Panel>
                     <br />
             <asp:Panel ID="Panel2" runat="server" Height="16px" Width="1400px" Style="float: left;">
                     <asp:LinkButton ID="LinkButton1" runat="server" Style="margin-left:0px;" OnClick="HomeButton1_Click" Font-Names="Microsoft Sans Serif" Font-Underline="False" ForeColor="Black">Home</asp:LinkButton>
                    <br />
                </asp:Panel>
            <br />
                <asp:Panel ID="Panel4" runat="server" Height="16px" Style="float: left;">
                    <hr style="width: 800px; height: -12px;" />
                    <br />
                       <br />
                </asp:Panel>
             <br />
                <asp:Panel ID="Panel10" runat="server" Height="42px" Style="float: left;" Width="200px">
                        <br />
                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Names="MS Reference Sans Serif" Font-Size="Large" Text="New Event"></asp:Label>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="Panel7" runat="server" Height="109px" Width="1393px">
                <br />
                <br />
                <br />
            </asp:Panel>
            <asp:Panel ID="Panel1" runat="server" Height="492px" Width="1291px"  style="margin-left:80px;">
                <%--login--%>
                <asp:Label ID="Label5" runat="server" Text="Event Name:" Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:TextBox ID="eventName" runat="server"  Font-Size="Large" Width="400px" Font-Names="Microsoft Sans Serif" ></asp:TextBox>
                <br />
                    <br />
                  <asp:Label ID="Label9" runat="server" Text="Start Time:" Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:TextBox ID="eventStartHour" runat="server"  Font-Size="Large" Width="32px" Font-Names="Microsoft Sans Serif" TextMode="DateTime" placeholder="HH"></asp:TextBox>
                    <asp:Label ID="Label12" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text=" : " Width="22px" Style="margin-left:10px;"></asp:Label>
                    <asp:TextBox ID="eventStartMinute" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" placeholder="MM"></asp:TextBox>
                 <br />
                <br />
                <asp:Label ID="Label10" runat="server" Text="End Time:"  Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                    <asp:TextBox ID="eventEndHour" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" placeholder="HH"></asp:TextBox>
                    <asp:Label ID="Label13" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Style="margin-left:10px;" Text=" : " Width="22px"></asp:Label>
                    <asp:TextBox ID="eventEndMinute" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" placeholder="MM"></asp:TextBox>
                <br />
                <br />
                          <asp:Label ID="Label8" runat="server" Text="Start Date:" Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:TextBox ID="startYear" runat="server"  Font-Size="Large" Width="80px" Font-Names="Microsoft Sans Serif" TextMode="DateTime" placeholder="yyyy"></asp:TextBox>
                    <asp:Label ID="Label11" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="-" Width="22px" Style="margin-left:10px;"></asp:Label>
                    <asp:TextBox ID="startMonth" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" placeholder="mm"></asp:TextBox>
                 <asp:Label ID="Label14" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="-" Width="22px" Style="margin-left:10px;" ></asp:Label>
                    <asp:TextBox ID="startDay" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" placeholder="dd"></asp:TextBox>
                 <br />
                <br />
                <asp:Label ID="Label16" runat="server" Text="End Date:"  Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                    <asp:TextBox ID="endYear" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="80px" placeholder="yyyy"></asp:TextBox>
                    <asp:Label ID="Label17" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Style="margin-left:10px;" Text="-" Width="22px"></asp:Label>
                    <asp:TextBox ID="endMonth" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" placeholder="mm"></asp:TextBox>
                <asp:Label ID="Label18" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Style="margin-left: 10px;" Text="-" Width="22px"></asp:Label>
                <asp:TextBox ID="endDay" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" placeholder="dd"></asp:TextBox>
                <br />
                 <br />
                <asp:Label ID="Label3" runat="server" Text="Recurrence:"  Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                    <asp:DropDownList ID="recDrop" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium">
                        <asp:ListItem Value="0">no recurrence</asp:ListItem>
                        <asp:ListItem Value="daily">daily</asp:ListItem>
                        <asp:ListItem>weekly</asp:ListItem>
                        <asp:ListItem>monthly</asp:ListItem>
                    </asp:DropDownList>  
                <br />
                     <br />
                 <asp:Label ID="Label20" runat="server" Text="Description:"  Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                    <asp:TextBox ID="description" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="MultiLine" Width="560px" Height="151px"></asp:TextBox>
                    <br />
                <br />
            </asp:Panel>
             <asp:Panel ID="Panel6" runat="server" Height="70px" Width="1393px" style="margin-left:545px;">
                <asp:Button ID="Button1" runat="server" Text="Submit" style="margin-left:150px;" Font-Names="Microsoft Sans Serif" Font-Size="Large" Width="100px" OnClick="Button1_Click"  />
                   <br />
                  <asp:Label ID="loginFail" runat="server" Font-Names="Microsoft Sans Serif" style="margin-left:140px;" Text="Please try again" Visible="False"></asp:Label>

                <br />
            </asp:Panel>
                 <asp:Panel ID="Panel9" runat="server" Height="16px" Style="float:left;" Width="594px">
                    <hr style="width: 800px; float:left; margin-left:0px;"/>
                    <br />
                </asp:Panel>
             <asp:Panel ID="Panel11" runat="server" Height="16px" Style="float:left;" Width="986px">
                <br />
                    <br />
                </asp:Panel>
                         <asp:Panel ID="Panel8" runat="server" Height="50px" Width="1393px" style="margin-left:180px;">
                <br />
            </asp:Panel>
<br />

        </div>
    </form>
</body>
</html>
