﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddProfessor.aspx.cs" Inherits="AddProfessor" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #form1 {
            height: 872px;
            width: 1146px;
        }
    </style>
</head>
<body style="margin-left: 10px; margin-bottom: 631px">
     <form id="form1" runat="server">
        <div style="height: 856px; margin-left: 120px; width: 909px;">
            <asp:Panel ID="Panel5" runat="server" Height="55px">
                <asp:Panel ID="Panel3" runat="server" Height="55px">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="MS Reference Sans Serif" Font-Size="XX-Large" Text="A+ Student Planner - Web"></asp:Label>
                    <br />
                </asp:Panel>
                        <br />
             <asp:Panel ID="Panel2" runat="server" Height="16px" Width="1400px" Style="float: left;">
                     <asp:LinkButton ID="LinkButton1" runat="server" Style="margin-left:0px;" OnClick="HomeButton1_Click" Font-Names="Microsoft Sans Serif" Font-Underline="False" ForeColor="Black">Home</asp:LinkButton>
                    <br />
                </asp:Panel>
            <br />
                <asp:Panel ID="Panel4" runat="server" Height="16px" Style="float: left;">
                    <hr style="width: 800px; height: -12px;" />
                    <br />
                       <br />
                </asp:Panel>
             <br />
                <asp:Panel ID="Panel10" runat="server" Height="42px" Style="float: left;" Width="200px">
                        <br />
                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Names="MS Reference Sans Serif" Font-Size="Large" Text="Add Professor"></asp:Label>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="Panel7" runat="server" Height="106px" Width="896px">
                <br />
                <br />
                <br />
            </asp:Panel>
            <asp:Panel ID="Panel1" runat="server" Height="573px" Width="784px"  style="margin-left:80px;">
                <%--login--%>
                <asp:Label ID="Title" runat="server" Text="Title:" Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:TextBox ID="TitleBox" runat="server"  Font-Size="Large" Width="400px" Font-Names="Microsoft Sans Serif" ></asp:TextBox>
                <br />
                    <br />
                <asp:Label ID="FirstName" runat="server" Text="First Name:"  Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif" style="margin-bottom: 0px"></asp:Label>
                <asp:TextBox ID="FirstNameBox" runat="server"  Font-Size="Large" Width="400px" Font-Names="Microsoft Sans Serif" ></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="LastName" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" style="margin-bottom: 0px" Text="Last Name:" Width="150px"></asp:Label>
                <asp:TextBox ID="LastNameBox" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Width="400px"></asp:TextBox>
                <br />
                    <br />
                <asp:Label ID="Email" runat="server" Text="Email:"  Font-Size="Large" Width="150px" Font-Names="Microsoft Sans Serif"></asp:Label>
                <asp:TextBox ID="EmailBox" runat="server"  Font-Size="Large" Width="400px" Font-Names="Microsoft Sans Serif" ></asp:TextBox>
                <br />
                    <br />
                  <asp:Label ID="OfficeLocation" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="Office Location:" Width="150px"></asp:Label>
                <asp:TextBox ID="OfficeLocationBox" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Width="400px"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="PhoneNumber" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="Phone Number:" Width="150px"></asp:Label>
                <asp:TextBox ID="NumberBox" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Width="400px"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label9" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="Office Start Time:" Width="150px"></asp:Label>
                <asp:TextBox ID="eventStartHour" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" PlaceHolder="HH"></asp:TextBox>
                <asp:Label ID="Label12" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Style="margin-left:10px;" Text=" : " Width="22px"></asp:Label>
                <asp:TextBox ID="eventStartMinute" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" PlaceHolder="MM"></asp:TextBox>
                <br />
                <br />
                <asp:Label ID="Label10" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="Office End Time:" Width="150px"></asp:Label>
                <asp:TextBox ID="eventEndHour" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" PlaceHolder="HH"></asp:TextBox>
                <asp:Label ID="Label13" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Style="margin-left:10px;" Text=" : " Width="22px"></asp:Label>
                <asp:TextBox ID="eventEndMinute" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" TextMode="DateTime" Width="32px" PlaceHolder="MM"></asp:TextBox>
                <br />
                 <br />
                <asp:Label ID="Label7" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Large" Text="Days:" Width="150px"></asp:Label>
                <asp:CheckBox ID="checkMon" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Style="margin-left:0px;" Text="Monday" />
                <br />
                <asp:CheckBox ID="CheckTues" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Style="margin-left:150px;" Text="Tuesday" />
                <br />
                <asp:CheckBox ID="CheckWed" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Style="margin-left:150px;" Text="Wednesday" />
                <br />
                <asp:CheckBox ID="CheckThurs" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Style="margin-left:150px;" Text="Thursday" />
                <br />
                <asp:CheckBox ID="CheckFri" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Style="margin-left:150px;" Text="Friday" />
                <br />
                <asp:CheckBox ID="CheckSat" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Style="margin-left:150px;" Text="Saturday" />
                <br />
                <asp:CheckBox ID="CheckSun" runat="server" Font-Names="Microsoft Sans Serif" Font-Size="Medium" Style="margin-left:150px;" Text="Sunday" />
                <br />
                 <br />
                <br />

            </asp:Panel>
             <asp:Panel ID="Panel6" runat="server" Height="70px" Width="691px" style="margin-left:180px;">
                <asp:Button ID="Submit" runat="server" Text="Submit" style="margin-left:370px;" Font-Names="Microsoft Sans Serif" Font-Size="Large" Width="100px" OnClick="Button1_Click"  />
                   <br />
                  <asp:Label ID="loginFail" runat="server" Font-Names="Microsoft Sans Serif" style="margin-left:370px;" Text="Please try again" Visible="False"></asp:Label>

                <br />
            </asp:Panel>
                 <asp:Panel ID="Panel9" runat="server" Height="16px" Style="float:left;" Width="594px">
                    <hr style="width: 800px; float:left; margin-left:0px;"/>
                    <br />
                </asp:Panel>
             <asp:Panel ID="Panel11" runat="server" Height="16px" Style="float:left;" Width="212px">
                <br />
                    <br />
                </asp:Panel>
                         <asp:Panel ID="Panel8" runat="server" Height="50px" Width="525px" style="margin-left:180px;">
                <br />
            </asp:Panel>
<br />

        </div>
    </form>
</body>
</html>
