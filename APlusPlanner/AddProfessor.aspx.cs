﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using MySql.Data;

public partial class AddProfessor : System.Web.UI.Page
{
    public MySqlConnection conn;
    public MySqlDataReader reader;
    public MySqlDataAdapter adapter;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoggedIn"] == null || Session["LoggedIn"] != "1")
        {
            Response.Redirect("LoginView.aspx");
        }
        conn = new MySqlConnection();
        conn.ConnectionString = "server=70.75.191.26; user id=root; password=; database=aplusplus;Allow Zero Datetime=True";
        conn.Open();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        MySqlCommand cmd;
        String starttime = eventStartHour.Text + ":" + eventStartMinute.Text;
        String endtime = eventEndHour.Text + ":" + eventEndMinute.Text;

        String officedays="";

        if (checkMon.Checked)
        {
            officedays+="Monday,";
        }
        if (CheckTues.Checked)
        {
            officedays += "Tuesday, ";
        }
        if (CheckWed.Checked)
        {
            officedays += "Wednesday, ";
        }
        if (CheckThurs.Checked)
        {
            officedays += "Thursday, ";
        }
        if (CheckFri.Checked)
        {
            officedays += "Friday";
        }
        String timestring = starttime + " - " + endtime;
        String query = "INSERT INTO professor(username,title,firstname,lastname,email,officelocation,phonenumber,officedays,officehours) values('"
            + Session["user"].ToString() + "','" + TitleBox.Text + "','"
            + FirstNameBox.Text + "','" + LastNameBox.Text + "','" + EmailBox.Text + "','"
            + OfficeLocationBox.Text + "','" + NumberBox.Text + "','" + officedays + "','" + timestring + "');";
        cmd = new MySqlCommand(query, conn);
        cmd.ExecuteReader();
        conn.Close();
        Response.Redirect("MainView.aspx");
    }

    protected void HomeButton1_Click(object sender, EventArgs e)
    {
        Response.Redirect("MainView.aspx");
    }
}