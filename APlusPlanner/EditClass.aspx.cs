﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using MySql.Data;
using DayPilot;
using DayPilot.Web.Ui.Recurrence;

public partial class Professor : System.Web.UI.Page
{
    public MySqlConnection conn;
    public MySqlDataReader reader;
    public MySqlDataAdapter adapter;
    public int id;

    public String redirect;
    String recurrence;

    protected void Page_Load(object sender, EventArgs e)
    {

        SemesterDropDown.Items.Clear();
        ClassDropDown.Items.Clear();  
        if (Session["LoggedIn"] == null || Session["LoggedIn"] != "1")
            {
                Response.Redirect("LoginView.aspx");
            }
            conn = new MySqlConnection();
            conn.ConnectionString = "server=70.75.191.26; user id=root; password=; database=aplusplus;Allow Zero Datetime=True";
            conn.Open();

        
            String query;
            query = "select * from class where user='" + Session["user"].ToString() + "';";

            MySqlCommand cmd = new MySqlCommand(query, conn);
            reader = cmd.ExecuteReader();
            String input = "";
            String classname = "";

            while (reader.Read())
            {
                input = reader.GetString(reader.GetOrdinal("Semester")) + ": " + reader.GetString(reader.GetOrdinal("ClassName")) + " - " + reader.GetString(reader.GetOrdinal("Prof"));
                classname = reader.GetString(reader.GetOrdinal("ClassName"));
                ClassDropDown.Items.Add(new ListItem(input, classname));
            }
            reader.Close();

            query = "select * from semester where username='" + Session["user"].ToString() + "';";

            cmd = new MySqlCommand(query, conn);
            reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                SemesterDropDown.Items.Add(reader.GetString(reader.GetOrdinal("name")));
            }

            reader.Close();
        
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        MySqlCommand cmd;
        String selectLast = "SELECT id FROM userevents ORDER BY ID DESC LIMIT 1;";
        cmd = new MySqlCommand(selectLast, conn);
        reader = cmd.ExecuteReader();
        reader.Read();
        id = reader.GetInt32(0);
        String eventId = id.ToString();
        reader.Close();

        conn = new MySqlConnection();
        conn.ConnectionString = "server=70.75.191.26; user id=root; password=; database=aplusplus";
        conn.Open();

        String namein = ClassDropDown.SelectedValue.ToString();

        String query;
        query = "select * from semester where username='" + Session["user"].ToString() + "' AND name='" + SemesterDropDown.SelectedValue.ToString() +"';";

        cmd = new MySqlCommand(query, conn);
        reader = cmd.ExecuteReader();

        reader.Read();

        String isMonday = "false";
        String isTuesday = "false";
        String isWednesday = "false";
        String isThursday = "false";
        String isFriday = "false";
        String isSaturday = "false";
        String isSunday = "false";
        String ClassName = namein;
        String prof = profBox.Text;
        String semester = SemesterDropDown.SelectedValue.ToString();
        String userName = Session["user"].ToString();

        String startyear;
        String startmonth;
        String startday;
        String endyear;
        String endday;
        String endmonth;


        startyear = reader.GetString(reader.GetOrdinal("startyear"));
        startmonth = reader.GetString(reader.GetOrdinal("startmonth"));
        startday = reader.GetString(reader.GetOrdinal("startday"));
        endyear = reader.GetString(reader.GetOrdinal("endyear"));
        endmonth = reader.GetString(reader.GetOrdinal("endmonth"));
        endday = reader.GetString(reader.GetOrdinal("endday"));
        
        
        reader.Close();

        //delete events associated with old events

        String deleteOldClassEvents = "DELETE FROM userevents WHERE username='" + userName + "' AND eventname='" + ClassName + "';";
        cmd = new MySqlCommand(deleteOldClassEvents, conn);
        reader = cmd.ExecuteReader();
        reader.Close();

        DateTime start = new DateTime(Int32.Parse(startyear), Int32.Parse(startmonth), Int32.Parse(startday), Int32.Parse(eventStartHour.Text), Int32.Parse(eventStartMinute.Text), 0);

        DateTime end = new DateTime(Int32.Parse(startyear), Int32.Parse(startmonth), Int32.Parse(startday), Int32.Parse(eventEndHour.Text), Int32.Parse(eventEndMinute.Text), 0);

        DateTime until = new DateTime(Int32.Parse(endyear), Int32.Parse(endmonth), Int32.Parse(endday), Int32.Parse(eventEndHour.Text), Int32.Parse(eventEndMinute.Text), 0);

        if (CheckMon.Checked)
        {
            isMonday = "true";
            DateTime tempendDate = end;
            DateTime tempDate = start;
            switch (start.DayOfWeek)
            { 
                case DayOfWeek.Monday:
                    break;
                case DayOfWeek.Tuesday:
                    start = start.AddDays(6);
                    end = end.AddDays(6);
                    break;
                case DayOfWeek.Wednesday:
                    start = start.AddDays(5);
                    end = end.AddDays(5);
                    break;
                case DayOfWeek.Thursday:
                    start = start.AddDays(4);
                    end = end.AddDays(4);
                    break;
                case DayOfWeek.Friday:
                    start = start.AddDays(3);
                    end = end.AddDays(3);
                    break;
                case DayOfWeek.Saturday:
                    start = start.AddDays(2);
                    end = end.AddDays(2);
                    break;
                case DayOfWeek.Sunday:
                    start = start.AddDays(1);
                    end = end.AddDays(1);
                    break;
            }
            id += 1;
            eventId = id.ToString();
            recurrence = RecurrenceRule.FromDateTime(id.ToString(), start).Weekly().Until(until).Encode();
            String query2 = "INSERT INTO userevents(id,username,eventname,tag,timestart,timeend,description,rec,untildate,semester) values('" + eventId + "','"
      + Session["user"].ToString() + "','" + namein + "','" + typeDrop.SelectedValue + "','" + start.ToString() + "','" + end.ToString() + "','"
      + description.Text + "','" + recurrence + "','" + until.ToString() + "','" + semester + "');";
    
          cmd = new MySqlCommand(query2, conn);
          reader = cmd.ExecuteReader();
          reader.Close();
          start = tempDate;
          end = tempendDate;
        }
        if (CheckTues.Checked)
        {
            isTuesday = "true";
            DateTime tempendDate = end;
            DateTime tempDate = start;
            switch (start.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    start = start.AddDays(1);
                    end = end.AddDays(1);
                    break;
                case DayOfWeek.Tuesday:
                    break;
                case DayOfWeek.Wednesday:
                    start = start.AddDays(6);
                    end = end.AddDays(6);
                    break;
                case DayOfWeek.Thursday:
                    start = start.AddDays(5);
                    end = end.AddDays(5);
                    break;
                case DayOfWeek.Friday:
                    start = start.AddDays(4);
                    end = end.AddDays(4);
                    break;
                case DayOfWeek.Saturday:
                    start = start.AddDays(3);
                    end = end.AddDays(3);
                    break;
                case DayOfWeek.Sunday:
                    start = start.AddDays(2);
                    end = end.AddDays(2);
                    break;
            }
            id += 1;
            eventId = id.ToString();
            recurrence = RecurrenceRule.FromDateTime(id.ToString(), start).Weekly().Until(until).Encode();
            String query2 = "INSERT INTO userevents(id,username,eventname,tag,timestart,timeend,description,rec,untildate,semester) values('" + eventId + "','"
      + Session["user"].ToString() + "','" + namein + "','" + typeDrop.SelectedValue + "','" + start.ToString() + "','" + end.ToString() + "','"
      + description.Text + "','" + recurrence + "','" + until.ToString() + "','" + semester + "');";
            cmd = new MySqlCommand(query2, conn);
            reader = cmd.ExecuteReader();
            reader.Close();
            start = tempDate;
            end = tempendDate;
        }
        if (CheckWed.Checked)
        {
            isWednesday = "true";
            DateTime tempendDate = end;
            DateTime tempDate = start;
            switch (start.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    start = start.AddDays(2);
                    end = end.AddDays(2);
                    break;
                case DayOfWeek.Tuesday:
                    start = start.AddDays(1);
                    end = end.AddDays(1);
                    break;
                case DayOfWeek.Wednesday:
                    break;
                case DayOfWeek.Thursday:
                    start = start.AddDays(6);
                    end = end.AddDays(6);
                    break;
                case DayOfWeek.Friday:
                    start = start.AddDays(5);
                    end = end.AddDays(5);
                    break;
                case DayOfWeek.Saturday:
                    start = start.AddDays(4);
                    end = end.AddDays(4);
                    break;
                case DayOfWeek.Sunday:
                    start = start.AddDays(3);
                    end = end.AddDays(3);
                    break;
            }


            id += 1;
            eventId = id.ToString();
            recurrence = RecurrenceRule.FromDateTime(id.ToString(), start).Weekly().Until(until).Encode();
            String query2 = "INSERT INTO userevents(id,username,eventname,tag,timestart,timeend,description,rec,untildate,semester) values('" + eventId + "','"
       + Session["user"].ToString() + "','" + namein + "','" + typeDrop.SelectedValue + "','" + start.ToString() + "','" + end.ToString() + "','"
       + description.Text + "','" + recurrence + "','" + until.ToString() + "','" + semester + "');";
            cmd = new MySqlCommand(query2, conn);
            reader = cmd.ExecuteReader();
            reader.Close();
            start = tempDate;
            end = tempendDate;
        }
       if (CheckThurs.Checked)
        {
            isThursday = "true";
            DateTime tempendDate = end;
            DateTime tempDate = start;
            switch (start.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    start = start.AddDays(3);
                    end = end.AddDays(3);
                    break;
                case DayOfWeek.Tuesday:
                    start = start.AddDays(2);
                    end = end.AddDays(2);
                    break;
                case DayOfWeek.Wednesday:
                    start = start.AddDays(1);
                    end = end.AddDays(1);
                    break;
                case DayOfWeek.Thursday:
                    break;
                case DayOfWeek.Friday:
                    start = start.AddDays(6);
                    end = end.AddDays(6);
                    break;
                case DayOfWeek.Saturday:
                    start = start.AddDays(5);
                    end = end.AddDays(5);
                    break;
                case DayOfWeek.Sunday:
                    start = start.AddDays(4);
                    end = end.AddDays(4);
                    break;
            }
            id += 1;
            eventId = id.ToString();
            recurrence = RecurrenceRule.FromDateTime(id.ToString(), start).Weekly().Until(until).Encode();
            String query2 = "INSERT INTO userevents(id,username,eventname,tag,timestart,timeend,description,rec,untildate,semester) values('" + eventId + "','"
         + Session["user"].ToString() + "','" + namein + "','" + typeDrop.SelectedValue + "','" + start.ToString() + "','" + end.ToString() + "','"
         + description.Text + "','" + recurrence + "','" + until.ToString() + "','" + semester + "');";
            cmd = new MySqlCommand(query2, conn);
            reader = cmd.ExecuteReader();
            reader.Close();
            start = tempDate;
            end = tempendDate;
       }
       if (CheckFri.Checked)
       {
           isFriday = "true";
           DateTime tempendDate = end;
           DateTime tempDate = start;
           switch (start.DayOfWeek)
           {
               case DayOfWeek.Monday:
                   start = start.AddDays(4);
                   end = end.AddDays(4);
                   break;
               case DayOfWeek.Tuesday:
                   start = start.AddDays(3);
                   end = end.AddDays(3);
                   break;
               case DayOfWeek.Wednesday:
                   start = start.AddDays(2);
                   end = end.AddDays(2);
                   break;
               case DayOfWeek.Thursday:
                   start = start.AddDays(1);
                   end = end.AddDays(1);
                   break;
               case DayOfWeek.Friday:
                   break;
               case DayOfWeek.Saturday:
                   start = start.AddDays(6);
                   end = end.AddDays(6);
                   break;
               case DayOfWeek.Sunday:
                   start = start.AddDays(5);
                   end = end.AddDays(5);
                   break;
           }
           id += 1;
           eventId = id.ToString();
           recurrence = RecurrenceRule.FromDateTime(id.ToString(), start).Weekly().Until(until).Encode();
           String query2 = "INSERT INTO userevents(id,username,eventname,tag,timestart,timeend,description,rec,untildate,semester) values('" + eventId + "','"
        + Session["user"].ToString() + "','" + namein + "','" + typeDrop.SelectedValue + "','" + start.ToString() + "','" + end.ToString() + "','"
        + description.Text + "','" + recurrence + "','" + until.ToString() + "','" + semester + "');";
           cmd = new MySqlCommand(query2, conn);
           reader = cmd.ExecuteReader();
           reader.Close();
           start = tempDate;
           end = tempendDate;
        }
        if (CheckSat.Checked)
        {
            isSaturday = "true";
            DateTime tempDate = start;
            DateTime tempendDate = end;
            switch (start.DayOfWeek)
            {
                case DayOfWeek.Monday:
                  start = start.AddDays(5);
                  end = end.AddDays(5);
                    break;
                case DayOfWeek.Tuesday:
                    start = start.AddDays(4);
                    end = end.AddDays(4);
                    break;
                case DayOfWeek.Wednesday:
                    start = start.AddDays(3);
                    end = end.AddDays(3);
                    break;
                case DayOfWeek.Thursday:
                    start = start.AddDays(2);
                    end = end.AddDays(2);
                    break;
                case DayOfWeek.Friday:
                   start = start.AddDays(1);
                   end = end.AddDays(1);
                    break;
                case DayOfWeek.Saturday:
                    break;
                case DayOfWeek.Sunday:
                    end = end.AddDays(6);
                   start = start.AddDays(6);
                    break;
            }
            id += 1;
            eventId = id.ToString();
            recurrence = RecurrenceRule.FromDateTime(id.ToString(), start).Weekly().Until(until).Encode();
          String query2 = "INSERT INTO userevents(id,username,eventname,tag,timestart,timeend,description,rec,untildate,semester) values('" + eventId + "','"
         + Session["user"].ToString() + "','" + namein + "','" + typeDrop.SelectedValue + "','" + start.ToString() + "','" + end.ToString() + "','"
         + description.Text + "','" + recurrence + "','" + until.ToString() + "','" + semester + "');";
            cmd = new MySqlCommand(query2, conn);
            reader = cmd.ExecuteReader();
            reader.Close();
            start = tempDate;
            end = tempendDate;
        }
       if (CheckSun.Checked)
        {
            isSunday = "true";
            DateTime tempendDate = end;
            DateTime tempDate = start;
            switch (start.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    start = start.AddDays(6);
                    end = end.AddDays(6);
                    break;
                case DayOfWeek.Tuesday:
                    start = start.AddDays(5);
                    end = end.AddDays(5);
                    break;
                case DayOfWeek.Wednesday:
                    start = start.AddDays(4);
                    end = end.AddDays(4);
                    break;
                case DayOfWeek.Thursday:
                    start = start.AddDays(3);
                    end = end.AddDays(3);
                    break;
                case DayOfWeek.Friday:
                    start = start.AddDays(2);
                    end = end.AddDays(2);
                    break;
                case DayOfWeek.Saturday:
                    start = start.AddDays(1);
                    end = end.AddDays(1);
                    break;
                case DayOfWeek.Sunday:
                    break;
            }
            id += 1;
            eventId = id.ToString();
            recurrence = RecurrenceRule.FromDateTime(id.ToString(), start).Weekly().Until(until).Encode();
            String query2 = "INSERT INTO userevents(id,username,eventname,tag,timestart,timeend,description,rec,untildate,semester) values('" + eventId + "','"
         + Session["user"].ToString() + "','" + namein + "','" + typeDrop.SelectedValue + "','" + start.ToString() + "','" + end.ToString() + "','"
         + description.Text + "','" + recurrence + "','" + until.ToString() + "','" + semester + "');";
            cmd = new MySqlCommand(query2, conn);
            reader = cmd.ExecuteReader();
            reader.Close();
            start = tempDate;
            end = tempendDate;
     
       }

       String addClass = "UPDATE class SET  Prof='" + prof + "', OnMonday='" + isMonday + "', OnTuesday='" + isTuesday +
           "', OnWednesday='" + isWednesday + "', OnThursday='" + isThursday + "', OnFriday='" + isFriday + "', Semester='" + semester
           + "' WHERE ClassName='" + ClassName + "' AND user='" + userName + "';";
       cmd = new MySqlCommand(addClass, conn);
       reader = cmd.ExecuteReader();
       reader.Close();

       conn.Close();
       Response.Redirect("MainView.aspx");
       
    }


    public void ASPNET_MsgBox(String message)
    {
        System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE=\"JavaScript\">" + System.Environment.NewLine);

        System.Web.HttpContext.Current.Response.Write("alert(\"" + message + "\")" + System.Environment.NewLine);

        System.Web.HttpContext.Current.Response.Write("</SCRIPT>");
    }
    protected void HomeButton1_Click(object sender, EventArgs e)
    {
        Response.Redirect("MainView.aspx");
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        conn = new MySqlConnection();
        conn.ConnectionString = "server=70.75.191.26; user id=root; password=; database=aplusplus;Allow Zero Datetime=True";
        conn.Open();

       

        String query;
        query = "select * from class where user='" + Session["user"].ToString() + "' and ClassName='" + ClassDropDown.SelectedValue.ToString() +"';";

        MySqlCommand cmd = new MySqlCommand(query, conn);
        reader = cmd.ExecuteReader();
        String input = "";
        String classname = "";
        String checkmon = "";
        String checktues = "";
        String checkwed = "";
        String checkthurs = "";
        String checkfri = "";
        String checksat = "";
        String checksun = "";


        while (reader.Read())
        {
            profBox.Text = reader.GetString(reader.GetOrdinal("Prof"));
            SemesterDropDown.SelectedValue = reader.GetString(reader.GetOrdinal("Semester"));
            checkmon = reader.GetString(reader.GetOrdinal("OnMonday"));
            checktues = reader.GetString(reader.GetOrdinal("OnTuesday"));
            checkwed = reader.GetString(reader.GetOrdinal("OnWednesday"));
            checkthurs = reader.GetString(reader.GetOrdinal("OnThursday"));
            checkfri = reader.GetString(reader.GetOrdinal("OnFriday"));
            checksat = reader.GetString(reader.GetOrdinal("OnSaturday"));
            checkmon = reader.GetString(reader.GetOrdinal("OnSunday"));
        }
        reader.Close();

        if (checkmon == "true")
        {
            CheckMon.Checked = true;
        }
        if (checktues == "true")
        {
            CheckTues.Checked = true;
        }
        if (checkwed == "true")
        {
            CheckWed.Checked = true;
        }
        if (checkthurs == "true")
        {
            CheckThurs.Checked = true;
        }
        if (checkfri == "true")
        {
            CheckFri.Checked = true;
        }
        if (checksat == "true")
        {
            CheckSat.Checked = true;
        }
        if (checksun == "true")
        {
            CheckSun.Checked = true;
        }

        query = "select * from userevents where username='" + Session["user"].ToString() + "' and eventname='" + ClassDropDown.SelectedValue.ToString() + "';";

        cmd = new MySqlCommand(query, conn);
        reader = cmd.ExecuteReader();

        while (reader.Read())
        {
            startLabel.Text = reader.GetString(reader.GetOrdinal("timestart"));
            endLabel.Text = reader.GetString(reader.GetOrdinal("timeend"));
            description.Text = reader.GetString(reader.GetOrdinal("description"));
            typeDrop.SelectedValue = reader.GetString(reader.GetOrdinal("tag"));
        }
        reader.Close();
        conn.Close();
    }
}